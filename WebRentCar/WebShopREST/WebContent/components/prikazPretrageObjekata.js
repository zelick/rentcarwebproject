Vue.component("prikaz-pretrage-objekata", { 
	data: function () {
	    return {
			saloni: null
	    }
	},
	template: `
	<div>
		<ul class="menu-bar">
 			<li>
 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
 			</li>
 			<li>
 				<a href="#" v-on:click="pretraga()">Pretrazi</a>
 			</li>
		</ul>
		<div style="margin: 60px">
		</div>
		<div v-for="s in saloni" class="salon-holder" v-on:click="prikazObjekta(s.id)" style="cursor: pointer">
			<div class="salon-image-border">
				<img :src="s.slika" class="salon-image">
			</div>
			<div class="salon-text">
				<p class="salon-name">{{s.naziv}}</p>
				<div class="salon-lokacija">
					<p class="adress-info">Adresa: {{s.lokacija.adresa}}</p>
					<p class="adress-coordinate">{{s.lokacija.duzina}}, {{s.lokacija.sirina}}</p>
				</div>
				<p class="salon-info">Prosečna ocena: {{s.ocena}}</p>
			</div>
		</div>
	</div>
	`,
    mounted () {
        axios
        .get('rest/saloni/pretraga')
        .then(response => {
			this.saloni = response.data
		});
    },
    methods: {
		natrag : function() {
			router.push(`/`);
    	},
    	pretraga : function(){
			router.push(`/pretragaObjekata`);
		},
		prikazObjekta : function(id) {
			router.push(`/prikaziObjekat/` + id);
		}
    }
});