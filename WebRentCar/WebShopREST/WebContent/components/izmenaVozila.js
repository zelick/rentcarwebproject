Vue.component("izmeni-vozilo", { 
	data: function () {
	    return {
			vozilo: {id:null, marka: null, model: null, cena: null, tip: null, vrsta: null,
			 			tipGoriva: null, brojVrata: null, brojOsoba:null, potrosnja: null, slika: null}, 
			prosledjenIdVozila: null, 
			porukaGreska: null
	    }
	},
	    template: `
	    	<div>
	    		<ul class="menu-bar">
					<li>
		 				<a href="#" v-on:click="pocetniEkran()">Pocetni Ekran</a>
		 			</li>
 				</ul>
	    	
	    	<div style="margin: 60px"></div>
		    <div class="form-container">
			    <h2>Izmena vozila</h2>
			    
			    <div class="form-group">
			      <label for="markaVozila">Marka:</label>
			      <input type="text" id="markaVozila" placeholder="marka" v-model="vozilo.marka">
			    </div>
			    
			    <div class="form-group">
			      <label for="model">Model:</label>
			      <input type="text" id="model" placeholder="model" v-model="vozilo.model">
			    </div>
			    
			    <div class="form-group">
			      <label for="cena">Cena:</label>
			      <input type="number" id="cena" placeholder="cena" v-model="vozilo.cena">
			    </div>
			    
			    <div class="form-group">
	                <label for="tip">Tip:</label>
	                <select id="tip" class="form-input-material" v-model="vozilo.tip">
	                    <option value="AUTO">AUTO</option>
	                    <option value="KOMBI">KOMBI</option>
	                    <option value="MOBILEHOME">MOBILEHOME</option>
               		</select>
            	</div>
			    
			    <div class="form-group">
	                <label for="vrsta">Vrsta:</label>
	                <select id="tip" class="form-input-material" v-model="vozilo.vrsta">
	                    <option value="MANUELNI">MANUELNI</option>
	                    <option value="AUTOMATIK">AUTOMATIK</option>
               		</select>
            	</div>
			    
			     <div class="form-group">
	                <label for="tipGoriva">Tip goriva:</label>
	                <select id="tipGoriva" class="form-input-material" v-model="vozilo.tipGoriva">
	                    <option value="BENZIN">BENZIN</option>
	                    <option value="DIZEL">DIZEL</option>
	                    <option value="HIBRID">HIBRID</option>
	                    <option value="ELEKTRICNI">ELEKTRICNI</option>
               		</select>
            	</div>
            	
            	<div class="form-group">
			      <label for="potrosnja">Potrosnja:</label>
			      <input type="number" id="potrosnja" placeholder="potrosnja" v-model="vozilo.potrosnja">
			    </div>
            	
               <div class="form-group">
			      <label for="brojVrata">Broj vrata:</label>
			      <input type="number" id="brojVrata" placeholder="broj vrata" v-model="vozilo.brojVrata">
			    </div>
			    
			     <div class="form-group">
			      <label for="brojOsoba">Broj osoba:</label>
			      <input type="number" id="brojOsoba" placeholder="broj osoba" v-model="vozilo.brojOsoba">
			    </div>
			    
			     <div class="form-group">
			      <label for="opis">Opis:</label>
			      <input type="text" id="opis" placeholder="opis" v-model="vozilo.opis">
			    </div>
            	
            	<div class="form-group">
			      <label for="slika">Slika:</label>
			      <input type="text" id="slika" placeholder="url slike" v-model="vozilo.slika">
			    </div>
			    <div>
			    	<label style="color: red;">{{porukaGreska}}</label>
			    </div>
			    <div>
            		<input type="submit" value="Izmeni" class="login-button" v-on:click="izmeni()">
            	</div>	
	  	</div>
	   </div>
	    `,
    mounted () {
        this.prosledjenIdVozila = this.$route.params.idVozila;
        axios
	    .get('rest/saloni/getVoziloById/' + this.prosledjenIdVozila)
	    .then(response => {
			this.vozilo = response.data;
		});
		
		this.porukaGreska = '';
    },
    methods: {
    	izmeni : function() {
			
			
			if(this.vozilo.marka == "" || this.vozilo.model == "" || this.vozilo.cena == "" 
				|| this.vozilo.brojVrata == "" || this.vozilo.brojOsoba == "" || this.vozilo.potrosnja == "" || this.vozilo.slika == ""){
						this.porukaGreska = 'Sva polja su obavezna!';
						return;
			}else{
				
				this.porukaGreska = '';
			}
	
			axios
			.post('rest/saloni/izmeniVozilo', this.vozilo);
			
			axios
			.post('rest/korisnici/izmeniVozilo', this.vozilo)
			.then(response => {
				router.push(`/pregledObjekta`);
			});
    	}, 
	    pocetniEkran: function(){
			router.push(`/`);
		}
    }
});