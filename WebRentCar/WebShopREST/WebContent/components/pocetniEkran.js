Vue.component("pocetni-ekran", { 
	data: function () {
	    return {
			saloni: null, 
			korisnik: { korpa: { cena: 0.0 } },
			loginout: 'Uloguj se',
			prikazVidljivost: 'display: none;',
			menadzeriVidljivost: 'display: none;',
			objekatVidljivost: 'display: none;',
			menazderObjekatVidljivost: 'display: none;',
			administratorVidljivot: 'display: none;',
			iznajmljivanjeVidljivost: 'display: none;',
			iznajmljivanjeVidljivostMenadzer: 'display: none;',
			showCart: false,
			showCartStyle: 'display: none;'
	    }
	},
	template: `
	<div>
		<ul class="menu-bar">
 			<li>
 				<a href="#" v-on:click="IzlogujSe()">{{loginout}}</a>
 			</li>
 			<li>
  				<a href="#" v-on:click="prikazProfila()" v-bind:style="prikazVidljivost">Profil</a>
  			</li>
 			<li>
  				<a href="#" v-on:click="kreirajMenadzera()" v-bind:style="menadzeriVidljivost">Dodaj Menadzera</a>
  			</li>
  			<li>
  				<a href="#" v-on:click="kreirajObjekat()" v-bind:style="objekatVidljivost">Dodaj Objekat</a>
  			</li>
  			<li>
  				<a href="#" v-on:click="pregledObjekta()" v-bind:style="menazderObjekatVidljivost">Moj Objekat</a>
  			</li>
 			<li>
 				<a href="#" v-on:click="PrikaziKorisnike()" v-bind:style="administratorVidljivot">Prikaz korisnika</a>
 			</li>
 			<li>
 				<a href="#" v-on:click="PrikazPorudzbinKorisnika()" v-bind:style="iznajmljivanjeVidljivost">Moje porudzbine</a>
 			</li>
 			<li>
 				<a href="#" v-on:click="PrikazPorudzbinMenadzera()" v-bind:style="iznajmljivanjeVidljivostMenadzer">Porudzbine</a>
 			</li>
 			<li>
 				<a href="#" v-on:click="Pretrazi()">Pretrazi</a>
 			</li>
		</ul>
 		
		<div style="margin: 60px"></div>
		<div v-for="s in saloni" class="salon-holder" v-on:click="prikazObjekta(s.id)" style="cursor: pointer">
			<div class="salon-image-border">
				<img :src="s.slika" class="salon-image">
			</div>
			<div class="salon-text">
				<p class="salon-name">{{s.naziv}}</p>
				<div class="salon-lokacija">
					<p class="adress-info">Adresa: {{s.lokacija.adresa}}</p>
					<p class="adress-coordinate">{{s.lokacija.duzina}}, {{s.lokacija.sirina}}</p>
				</div>
				<p class="salon-info">Prosečna ocena: {{s.ocena}}</p>
			</div>
		</div>
	</div>
	`,
    mounted () {
		axios
		.post('rest/korisnici/test')
		.then(response => {
			if(response.status === 200) {
				
				console.log("Prosao");
			}
		});
		
        axios
        .get('rest/saloni/')
        .then(response => {
			this.saloni = response.data
		});
		
		axios
		.get('rest/korisnici/currentUser')
		.then(response => {
			this.korisnik = response.data
		
			if(this.korisnik == null || this.korisnik == "") {
				this.prikazVidljivost = 'display: none';
				this.loginout = 'Uloguj se'
			}
			else if (this.korisnik.uloga == "ADMINISTRATOR") {
				this.menadzeriVidljivost = '';
				this.objekatVidljivost = '';
				this.loginout = 'Izloguj se';
				this.prikazVidljivost = '';
				this.administratorVidljivot = '';
			}
			else if (this.korisnik.uloga == "MENADZER") {
				this.menazderObjekatVidljivost = '';
				this.loginout = 'Izloguj se';
				this.prikazVidljivost = '';
				this.iznajmljivanjeVidljivostMenadzer = '';
			}
			else {
				this.loginout = 'Izloguj se';
				this.prikazVidljivost = '';
				this.iznajmljivanjeVidljivost = '';
			}
		});
		
		//prikaziSveKorsinike
    },
    methods: {
    	prikazProfila : function() {
			router.push(`/prikazProfila`);
    	}, 
    	IzlogujSe : function(){
		
			//isprzni korpu 
			axios.post('rest/korisnici/isprazniKorpu/' + this.korisnik.id);
	
			axios
			.post('rest/korisnici/logout')
			.then(response => {
				
				router.push(`/logovanje`);
			});
		},
		prikazObjekta : function(id) {
			router.push(`/prikaziObjekat/` + id);
		},
		Pretrazi : function(){
			router.push(`/pretragaObjekata`);
		},
		kreirajMenadzera : function() {
			router.push(`/registracijaMenadzera`);
		},
		kreirajObjekat : function() {
			
			axios
			.get('rest/korisnici/proveriDostupnostMenadzera')
			.then(response => {
				const slobodan = response.data;
				if(slobodan === false) {
					router.push(`/registracijaMenadzera`);
				}
				else {
					router.push(`/kreirajObjekat`);
				}
			});
		},
		pregledObjekta : function() {
			if(this.korisnik.salon.naziv != null && this.korisnik.salon.naziv != ""){
				router.push(`/pregledObjekta`);
			}
		},
		toggleCart : function() {
			this.showCart = !this.showCart;
			if(this.showCart) {
				this.showCartStyle = '';
			}
			else {
				this.showCartStyle = 'display: none;';
			}
		},
		cenaUKorpi : function() {
			if(this.korisnik == null || this.korisnik == "") {
				return 0;
			}
			else {
				return this.korisnik.korpa.cena;
			}
		}, 
		PrikaziKorisnike : function(){
			router.push(`/prikaziSveKorsinike`);
		}, 
		PrikazPorudzbinKorisnika : function(){
			router.push(`/prikazPorudzbinaKorisnika`);
		}, 
		PrikazPorudzbinMenadzera : function(){
			router.push(`/prikazPorudzbinaMenadzera`);	
		}
    }
});