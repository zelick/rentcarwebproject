Vue.component("prikazProfila-korisnika", { 
	data: function () {
	    return {
			korisnik: null,
			proveriLozinku: null, 
			prosledjenId: null, 
			greskaLozinka: null
	    }
	},
	    template: 
	    `
	    <div>
	    <ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
 			</li>
		</ul>
	    
	    <div class="form-container">
        <h3>Profil korisnika</h3>
        <div>
            <div class="form-group form-group-inline">
                <label for="korisnickoIme">Korisničko ime:</label>
                <input type="text" id="korisnickoIme" class="form-input-material" v-model="korisnik.korisnickoIme">
            </div>
            <div class="form-group form-group-inline">
                <label for="lozinka">Lozinka:</label>
                <input type="password" id="lozinka" class="form-input-material" v-model="korisnik.lozinka">
            </div>
            <div class="form-group form-group-inline">
                <label for="proveriLozinku">Potvrdi lozinku:</label>
                <input type="password" id="proveriLozinku" class="form-input-material" v-model="proveriLozinku" required>
                <label style="color: red;">{{greskaLozinka}}</label>
            </div>
            <div class="form-group form-group-inline">
                <label for="ime">Ime:</label>
                <input type="text" id="ime" class="form-input-material" v-model="korisnik.ime"  disabled>
            </div>
            <div class="form-group form-group-inline">
                <label for="prezime">Prezime:</label>
                <input type="text" id="prezime" class="form-input-material" v-model="korisnik.prezime" disabled>
            </div>
            <div class="form-group form-group-inline">
                <label for="datum">Datum rođenja:</label>
                <input type="text" id="datum" class="form-input-material" v-model="korisnik.datumRodjenja" disabled>
            </div>
            <div class="form-group form-group-inline">
                <label for="pol">Pol:</label>
                <select id="pol" class="form-input-material" v-model="korisnik.pol" disabled>
                    <option value="M">MUSKI</option>
                    <option value="Z">ZENSKI</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" value="Izmeni" class="btn" v-on:click="izmena()">
            </div>
        </div>
    </div>
    </div>`,
    mounted () {
		
		axios
		.get('rest/korisnici/currentUser')
		.then(response => {
			this.korisnik = response.data
		});
    },
    methods: {
    	izmena : function() {
			
			if(this.proveriLozinku == null) {
				this.greskaLozinka = 'Lozinke nisu jednake!';
				//return;
			}
			
			if(this.korisnik.lozinka != this.proveriLozinku) {
				this.greskaLozinka = 'Lozinke nisu jednake!';
				//return;
			}
			else {
				this.greskaLozinka = '';
				
				axios
				.put('rest/korisnici/update', this.korisnik)
				.then(response => {
					router.push(`/`)
				});
			}
    	},
    	natrag : function() {
			router.push(`/`);
    	}
    }
});