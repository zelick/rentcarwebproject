Vue.component("prikaz-svih-korisnika", { 
	data: function () {
	    return {
		   korisiniciDto: null
	    }
	},
	    template: `
	    <div>
	      <ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="pocetniEkran()">Pocetni Ekran</a>
 			</li>
 			<li>
 				<a href="#" v-on:click="PretragaKorisnika()">Pretraga korisnika</a>
 			</li>
 			<li>
 				<a href="#" v-on:click="prikaziSumnjive()">Prikaz sumnjivih</a>
 			</li>
 			<li>
 				<a href="#" v-on:click="prikaziSve()">Prikazi sve</a>
 			</li>
		  </ul>
		  <div class="table-container">
		  <table>
		    <tr>
		      <th>KORISNICKO IME</th>
		      <th>IME</th>
		      <th>PREZIME</th>
		      <th>POL</th>
		      <th>DATUM RODJENJA</th>
		      <th>ULOGA</th>
		      <th>BODOVI</th>
		      <th>TIP</th>
		      <th>AKCIJE</th>
		    </tr>
		    <tr v-for="k in korisiniciDto">
		      <td>{{ k.korisnickoIme }}</td>
		      <td>{{ k.ime }}</td>
		      <td>{{ k.prezime }}</td>
		      <td>{{ k.pol }}</td>
		      <td>{{ k.datumRodjenja }}</td>
		      <td>{{ k.uloga }}</td>
		      <td>{{ k.bodovi }}</td>
		      <td>{{ k.tipKupca.ime }}</td>
		      <td>
		      	<button class="porudzbina-button" v-if="canBlock(k)"  v-on:click="blokirajKorisnika(k)">BLOKIRAJ</button>
		      </td>
		    </tr>
		  </table>
		  </div>
		</div>	    
	    `,
    mounted () {
        axios.get('rest/korisnici/svikorisniciDTO')
        .then(response => {
			this.korisiniciDto = response.data;
		});
    },
    methods: {
		pocetniEkran: function(){
			router.push(`/`);
		}, 
		PretragaKorisnika: function(){
			router.push(`/pretragaKorisnika`);
		}, 
		canBlock: function(kupac){
			if(kupac.uloga == "ADMINISTRATOR"){
				return false;
			}else{
				return true;
			}
		}, 
		blokirajKorisnika: function(korisnik){
			axios.post('rest/korisnici/blokirajKorisnika/' + korisnik.id);
		}, 
		prikaziSumnjive: function(){
			var listaSumnjivihDto = [];
            var requests = [];

            for(var obj of this.korisiniciDto) {
                if(obj.uloga === "KUPAC") {

                    var zahtev = obj.id;
                    const idx = obj;

                    requests.push(
                        axios
                        .get('rest/porudzbine/daLiJeSumnjiv/' + zahtev)
                        .then(response => {
                            if(response.data === true){
                                listaSumnjivihDto.push(idx);
                            }
                        })
                    );
                }
            }

            Promise
            .all(requests)
              .then(() => {
                this.korisiniciDto = listaSumnjivihDto;
            });

            this.korisiniciDto = listaSumnjivihDto;
		}, 
		prikaziSve: function(){
			 axios.get('rest/korisnici/svikorisniciDTO')
	        .then(response => {
				this.korisiniciDto = response.data;
			});
		}
    }
});