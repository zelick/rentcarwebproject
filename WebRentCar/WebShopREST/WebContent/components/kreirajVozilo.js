Vue.component("kreiraj-vozilo", { 
	data: function () {
	    return {
			vozilo: {id:null, marka: null, model: null, cena: null, tip: null, vrsta: null,
			 			tipGoriva: null, brojVrata: null, brojOsoba:null, potrosnja: null, slika: null}, 
			idSalona: null, 
			ulogovanKorisnik: null, 
			porukaGreska: null
	    }
	},
	    template: `
	    	<div>
	    	  <ul class="menu-bar">
				<li>
	 				<a href="#" v-on:click="pocetniEkran()">Pocetni Ekran</a>
	 			</li>
 			</ul>
 			
 			<div style="margin: 60px"></div>
		    <div class="form-container">
			    <h2>Dodaj vozilo</h2>
			    
			    <div class="form-group">
			      <label for="markaVozila">Marka:</label>
			      <input type="text" id="markaVozila" placeholder="marka" v-model="vozilo.marka">
			    </div>
			    
			    <div class="form-group">
			      <label for="model">Model:</label>
			      <input type="text" id="model" placeholder="model" v-model="vozilo.model">
			    </div>
			    
			    <div class="form-group">
			      <label for="cena">Cena:</label>
			      <input type="number" id="cena" placeholder="cena" v-model="vozilo.cena">
			    </div>
			    
			    <div class="form-group">
	                <label for="tip">Tip:</label>
	                <select id="tip" class="form-input-material" v-model="vozilo.tip">
	                    <option value="AUTO">AUTO</option>
	                    <option value="KOMBI">KOMBI</option>
	                    <option value="MOBILEHOME">MOBILEHOME</option>
               		</select>
            	</div>
			    
			    <div class="form-group">
	                <label for="vrsta">Vrsta:</label>
	                <select id="tip" class="form-input-material" v-model="vozilo.vrsta">
	                    <option value="MANUELNI">MANUELNI</option>
	                    <option value="AUTOMATIK">AUTOMATIK</option>
               		</select>
            	</div>
			    
			     <div class="form-group">
	                <label for="tipGoriva">Tip goriva:</label>
	                <select id="tipGoriva" class="form-input-material" v-model="vozilo.tipGoriva">
	                    <option value="BENZIN">BENZIN</option>
	                    <option value="DIZEL">DIZEL</option>
	                    <option value="HIBRID">HIBRID</option>
	                    <option value="ELEKTRICNI">ELEKTRICNI</option>
               		</select>
            	</div>
            	
            	<div class="form-group">
			      <label for="potrosnja">Potrosnja:</label>
			      <input type="number" id="potrosnja" placeholder="potrosnja" v-model="vozilo.potrosnja">
			    </div>
            	
               <div class="form-group">
			      <label for="brojVrata">Broj vrata:</label>
			      <input type="number" id="brojVrata" placeholder="broj vrata" v-model="vozilo.brojVrata">
			    </div>
			    
			     <div class="form-group">
			      <label for="brojOsoba">Broj osoba:</label>
			      <input type="number" id="brojOsoba" placeholder="broj osoba" v-model="vozilo.brojOsoba">
			    </div>
			    
			     <div class="form-group">
			      <label for="opis">Opis:</label>
			      <input type="text" id="opis" placeholder="opis" v-model="vozilo.opis">
			    </div>
            	
            	<div class="form-group">
			      <label for="slika">Slika:</label>
			      <input type="text" id="slika" placeholder="url slike" v-model="vozilo.slika">
			    </div>
			    <div>
			    	<label style="color: red;">{{porukaGreska}}</label>
			    </div>
			    <div>
			    	<input type="submit" value="Kreiraj" class="login-button" v-on:click="kreiraj()">
			    </div>
            	
	  	</div>
	   </div>
	    `,
    mounted () {
		this.porukaGreska = '';
    },
    methods: {
    	kreiraj : function() {
			this.idSalona = this.$route.params.idObjekta;
			
			//opis nije obavezan
			if(this.vozilo.marka == null || this.vozilo.model == null || this.vozilo.cena == null 
					|| this.vozilo.tip == null || this.vozilo.vrsta == null || this.vozilo.tipGoriva == null 
					|| this.vozilo.brojVrata == null || this.vozilo.brojOsoba == null || this.vozilo.potrosnja == null || this.vozilo.slika == null){
						this.porukaGreska = 'Sva polja su obavezna!';
						return;
			}else{
				this.porukaGreska = '';
			}
			
			//dodatno
			if(this.vozilo.marka == "" || this.vozilo.model == "" || this.vozilo.cena == "" 
				|| this.vozilo.brojVrata == "" || this.vozilo.brojOsoba == "" || this.vozilo.potrosnja == "" || this.vozilo.slika == ""){
						this.porukaGreska = 'Sva polja su obavezna!';
						return;
			}else{
				
				this.porukaGreska = '';
			}
			
			axios
			.post('rest/saloni/dodajVozilo/' + this.idSalona, this.vozilo);
			/*.then(response => {
			    this.idTmp = response.data;
	 		 }); ovo ne radi iz nepoznatog razloga*/ 
		
			axios
			.get('rest/korisnici/currentUser')
			.then(response => {
				
				this.ulogovanKorisnik = response.data;

				if(this.ulogovanKorisnik != null) {
					axios
					.post('rest/korisnici/dodajVozilo/' + this.ulogovanKorisnik.id, this.vozilo)
					.then(response => {
						router.push(`/pregledObjekta`);
					});
				}
			});
    	}, 
	    pocetniEkran: function(){
			router.push(`/`);
		}
    }
});