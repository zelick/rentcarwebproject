Vue.component("prikaz-objekta", { 
	data: function () {
	    return {
			salon: { naziv: '', lokacija: { adresa: '' }, vozila: [] },
			prosledjenId: null, 
			status: "zatvoren",
			latitude: 45.22404770640237,
			longitude: 20.219918785415626,
			showCart: false,
			showCartStyle: 'display: none;',
			korisnik: { korpa: { vozila: [] }, cena: 0.0},
			komentari: null,
			dostupnaVozila: null,
			pocetniDatum: null,
			krajnjiDatum: null,
			showVozila: false,
			shouldShowCartItems: false,
			omogucenoDodavanjeUKorpu: false
	    }
	},
	template: `
	<div>
		<ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
 			</li>
		</ul>
		
		<div id="message" class="message"></div>
		
		<button class="cart-button" v-on:click="toggleCart()">
			<span class="text">Korpa</span>
		  	<span class="icon">
		    	<img src="https://static.vecteezy.com/system/resources/previews/019/787/018/original/shopping-cart-icon-shopping-basket-on-transparent-background-free-png.png" alt="Shopping Cart Icon" width="40" height="30">
		  	</span>
		</button>
		
		<div class="cart-container" v-bind:style="showCartStyle">
    		<h2 style="margin-bottom: 14px;">Pregled korpe</h2>
    		
    		<div class="cart-items">
          		<div class="cart-item" v-for="vozilo in korisnik.korpa.vozila">
            		<div class="image-box">
              			<img :src="vozilo.slika" style="height: 100px;" />
            		</div>
            		<div class="about">
              			<h1 class="title">{{ vozilo.marka }} {{ vozilo.model }}</h1>
              			<h3 class="subtitle">Cena: {{ vozilo.cena }} din na dan</h3>
              			<h3 class="subtitle">{{ vozilo.tip }} {{ vozilo.vrsta }}</h3>
              			<button v-on:click="ukloniVozilo(vozilo)">Ukloni</button>
            		</div>
          		</div>
        	</div>
    		
    		<p style="margin-top: 8px; margin-bottom: 8px;" v-if="!shouldShowCartItems">Korpa je prazna</p>
    		
    		<div class="cart-controls">
    			<p style="margin-right: auto;">Cena: {{cenaUKorpi()}} din na dan</p>
    			<button class="cart-poruci" v-on:click="kreirajPorudzbinu()">Iznajmi</button>
    		</div>
  		</div>

		<div style="margin: 60px"></div>
		<div class="salon-holder">
			<div class="salon-image-border">
				<img :src="salon.slika" class="salon-image">
			</div>
			<div class="salon-text">
				<p class="salon-name">{{salon.naziv}}</p>
				<p class="salon-info">{{salon.lokacija.adresa}}</p>
				<p class="salon-info">{{status}}</p>
				<p class="salon-info">Radno vreme: {{salon.radnoVreme}}</p>
				<p class="salon-info">Ocena: {{salon.ocena}}</p>
			</div>
		</div>
		
		<h2 class="salon-heading">Ponuda vozila</h2>
		
		<div class="holder-pretraga">
		  <div>
		    <label for="start-date-picker">Izaberi početni datum:</label>
		    <input type="date" id="start-date-picker" v-model="pocetniDatum">
		  </div>
		  <div>
		    <label for="end-date-picker">Izaberi krajnji datum:</label>
		    <input type="date" id="end-date-picker" v-model="krajnjiDatum">
		  </div>
		  <div>
		    <button class="login-button" v-on:click="pretragaVozila()">Pretraga</button>
		  </div>
	   	</div>
		
		<div class="salon-holder" v-for="s in dostupnaVozila">
			<div class="salon-image-border">
				<img :src="s.slika" class="salon-image">
			</div>
			<div class="salon-text">
				<p class="salon-name">{{s.marka}} {{s.model}}</p>
				<p class="salon-info">Cena: {{s.cena}}din na dan</p>
				<p class="salon-info">{{s.tip}} {{s.vrsta}}</p>
				<p class="salon-info">{{s.tipGoriva}} {{s.potrosnja}}l/100km</p>
				<p class="salon-info">Opis: {{s.opis}}</p>
				<p class="salon-info">{{slobodanIliNe(s.status)}}</p>
			</div>
			<button class="button-dodajUkorpu-vozilo" v-on:click="dodajUKorpu(s)" v-if="omogucenoDodavanjeUKorpu">Dodaj u korpu</button>
		</div>
		
		<h2 class="salon-heading">Lokacija</h2>
		<div id="map" class="map-container"></div>
		
		<h2 class="salon-heading">Komentari</h2>
		<div class="komentar-holder" v-for="k in komentari">
		<div class="salon-text">
			<p class="salon-name">{{k.korisnik.korisnickoIme}}</p>
			<p class="salon-info">{{k.tekst}}</p>
			<p class="salon-info">Ocena: {{k.ocena}}</p>
		</div>
		</div>
	</div>
	`,
    mounted () {
		window.scrollTo(0, 0);
		
		axios
		.get('rest/korisnici/currentUser')
		.then(response => {
			if(response.data != "") {
				
				this.korisnik = response.data;
			}
		});
		
		this.prosledjenId = this.$route.params.id;
        axios
        .get('rest/saloni/id/' + this.prosledjenId)
        .then(response => {
			this.salon = response.data;
			this.dostupnaVozila = this.salon.vozila;
			this.sortiraj();
			
			if(this.salon.status == "true"){
				this.status = "Otvoren";
			}
			
			this.latitude = this.salon.lokacija.duzina;
			this.longitude = this.salon.lokacija.sirina;
			
			var proj4 = window.proj4;
			proj4.defs('EPSG:4326', '+proj=longlat +datum=WGS84 +no_defs');
			
			ol.proj.proj4.register(proj4);
			
			console.log(this.longitude, this.latitude);
			
			const map = new ol.Map({
          		target: 'map',
          		layers: [
            		new ol.layer.Tile({
              			source: new ol.source.OSM()
            		})
          		],
          		view: new ol.View({
            		center: ol.proj.fromLonLat([this.longitude, this.latitude]),
            		zoom: 15
          		})
        	});
        	
        	const marker = new ol.Overlay({
  				position: ol.proj.fromLonLat([this.longitude, this.latitude]),
  				positioning: 'center-center',
  				element: document.createElement('div'),
  				stopEvent: false
			});

			map.addOverlay(marker);

			marker.getElement().innerHTML = '<img src="https://cdn0.iconfinder.com/data/icons/map-location-solid-style/91/Map_-_Location_Solid_Style_01-1024.png" alt="Pin" style="width: 42px; height: 42px;">';
		
			if (this.korisnik.uloga === "ADMINISTRATOR" || this.korisnik.uloga === "MENADZER") {
				axios
				.get('rest/komentari/komentariSalonaSvi/' + this.salon.id)
				.then(response => {
					this.komentari = response.data;
				});
			}
			else {
				axios
				.get('rest/komentari/komentariSalonaVidljivi/' + this.salon.id)
				.then(response => {
					this.komentari = response.data;
				});
			}
			
		});	
    },
    methods: {
    	natrag : function() {
			router.push(`/`);
    	},
    	slobodanIliNe : function(txt) {
			if(txt === true) {
				return 'Dostupan';
			}
			return 'Zauzet';
		},
		toggleCart : function() {
			this.showCart = !this.showCart;
			if(this.showCart) {
				this.showCartStyle = '';
			}
			else {
				this.showCartStyle = 'display: none;';
			}
		},
		cenaUKorpi : function() {
			if(this.korisnik == null || this.korisnik == "") {
				return 0;
			}
			else {
				return this.korisnik.korpa.cena;
			}
		},
		pretragaVozila : function() {
			
			var startDate = new Date(this.pocetniDatum);
    		var endDate = new Date(this.krajnjiDatum);
    		var today = new Date();
			
			if (this.pocetniDatum == null || this.krajnjiDatum == null) {
				this.showMessage('Unesite datume!');
			}
			else if (startDate < endDate && startDate >= today) {
				this.omogucenoDodavanjeUKorpu = true;
			axios
			.get('rest/saloni/dostupnaVozila/' + this.salon.id)
			.then(response => {
				
				this.dostupnaVozila = response.data;
				var listaVozila = [];
				var requests = [];
				
				for(var obj of this.dostupnaVozila) {
					
					var zahtev = this.pocetniDatum + '&' + this.krajnjiDatum + '&' + obj.id;
					const idx = obj;
					
					requests.push(
						axios
						.get('rest/porudzbine/dostupnostVozila/' + zahtev)
						.then(response => {
							if(response.data === true) {
								listaVozila.push(idx);
							}
						})
					);
				}
				
				this.omogucenoDodavanjeUKorpu = true;
				
				Promise
				.all(requests)
      			.then(() => {
					this.dostupnaVozila = listaVozila;
					this.sortiraj();
				});
			});
			
			this.sortiraj();
			}
			else {
				this.showMessage('Datumi nisu korektno uneti!');
			}
		},
		dodajUKorpu : function(vozilo) {
			if (this.pocetniDatum == null || this.krajnjiDatum == null) {
				console.log("Nisu odabrani datumi");
				this.showMessage('Nisu odabrani datumi!');
			}
			else if (vozilo.status === true && this.korisnik.id != null) {
				this.korisnik.korpa.vozila.push(vozilo);
				this.korisnik.korpa.cena += vozilo.cena;
				
				const index = this.dostupnaVozila.findIndex(v => v.id === vozilo.id); //izbaci vozilo koje je dodato u korpu
                if (index !== -1) {
					this.dostupnaVozila.splice(index, 1);
                }
				
				this.izmeniVidljivostKorpe();
				this.sortiraj();
			}
		},
		izmeniVidljivostKorpe : function() {
			
			if(this.korisnik.korpa.cena == 0) {
				this.shouldShowCartItems = false;
			}
			else {
				this.shouldShowCartItems = true;
			}
		},
		ukloniVozilo : function(vozilo) {
			const index = this.korisnik.korpa.vozila.findIndex(v => v.id === vozilo.id);
            if (index !== -1) {
				this.korisnik.korpa.vozila.splice(index, 1);
				this.korisnik.korpa.cena -= vozilo.cena;
				
				this.dostupnaVozila.push(vozilo);
				this.izmeniVidljivostKorpe();
				this.sortiraj();
            }
		},
		kreirajPorudzbinu : function() {
			
			if (this.korisnik.korpa.cena > 0) {
			var porudzbina = {
				id: this.korisnik.id,
				vozila: this.korisnik.korpa.vozila,
				salon: this.salon,
				cena: this.korisnik.korpa.cena,
				kupac: this.korisnik,
				datumIVreme: this.pocetniDatum,
				krajnjiDatum: this.krajnjiDatum
			};
			
			axios
			.post('rest/porudzbine/kreirajPrivremeno', porudzbina)
			.then(response => {
				console.log(response.data);
				
				router.push(`/pregledKorpe`);
			});
			}
			else {
				this.showMessage('Korpa je prazna!');
			}
		},
		sortiraj : function() {
			var niz = this.dostupnaVozila;
			
			niz.sort(function(a, b) {
				return a.id - b.id;
			});
			
			this.dostupnaVozila = niz;
		},
		showMessage : function(message) {
  			var messageElement = document.getElementById('message');
  			messageElement.textContent = message;
  			messageElement.style.display = 'block';

  			// Remove the message element after a certain duration (optional)
  			setTimeout(function() {
    			messageElement.style.display = 'none';
  			}, 3000); // Adjust the timeout duration as needed
		}
    }
});