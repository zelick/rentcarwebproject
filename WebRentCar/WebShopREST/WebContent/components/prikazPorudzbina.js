Vue.component("prikaz-porudzbina", { 
	data: function () {
	    return {
			porudzbine: [], 
			korisnik: null,
			odabranaPorudzbina: { ocena: 0 },
			komentar: "",
			showForm: false,
      		stars: [1, 2, 3, 4, 5],
      		hoveredStar: 0,
			nazivSalona: "", 
			pocetniDatum: "", 
			krajnjiDatum: "", 
			cenaOd: "", 
			cenaDo: "", 
			selectedSort: "nazivRastuce"
	    };
	},
	computed: {
    	starColorClasses : function () {
      		const selectedRating = this.odabranaPorudzbina.ocena;
      		return this.stars.map((star) => ({
        		star,
        		filled: star <= selectedRating,
        		hovered: star <= this.hoveredStar,
      		}));
    	},
  	},
	template: `
	    	<div class="sve-porudzbine">
	    		<ul class="menu-bar">
					<li>
		 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
		 			</li>
				</ul>
				
				<div style="margin: 60px"></div>
				<h2 class="salon-heading">Pretraga porudzbina</h2>
			    <div style="margin: 20px"></div>
			    
				<div class="holder-pretraga-poruzbina">
				
					  <div>
					 	<label>Naziv salona:</label>
					 	<input type="text" v-model="nazivSalona">
					 </div>
				
					  <div>
					    <label for="start-date-picker">Datum od:</label>
					    <input type="date" id="start-date-picker" v-model="pocetniDatum">
					  </div>
					  
					  <div>
					    <label for="end-date-picker">Datum do:</label>
					    <input type="date" id="end-date-picker" v-model="krajnjiDatum">
				  	  </div>
				  	  
				  	  <div>
				  	  	<label>Cena od:</label>
				  	  	<input type="number" v-model="cenaOd">
				  	  </div>
					  
					  <div>	
					    <label >Cena do:</label>
					    <input type="number" v-model="cenaDo">
				  	  </div>
				  	  
					  <div>
					  	 <label>Sortiraj po:</label>
					  	 <select id="sort" class="porudzbina-sort" v-model="selectedSort">
		                    <option value="nazivRastuce" selected="selected">NAZIV OBJEKTA RASTUCE</option>
		                    <option value="nazivOpadajuce">NAZIV OBJKETA OPADAJUCE</option>
		                    <option value="cenaRastuce">CENA RASTUCE</option>
		                    <option value="cenaOpadajuce">CENA OPADAJUCE</option>
		                    <option value="datumRastuce">DATUM RASTUCE</option>
		                    <option value="datumOpadajuce">DATUM OPADAJUCE</option>
	                	</select>
					  </div>
					  <div>
					    <button class="login-button" v-on:click="pretragaPorudzbine()">Pretraga</button>
					  </div>
	   			</div>
				
				<div style="margin: 20px"></div>
				<h2 class="salon-heading">Porudzbine</h2>
				<div style="margin: 20px"></div>
				
				<div class="porudzbina-holder" v-for="p in porudzbine" :key="p.id">
			    	<div>
			    		<p class="porudzbina-salon-name">{{p.salon.naziv}}</p>
			    		<p class="porudzbina-salon-info">Datum iznajmljivanja: {{p.datumIVreme}}</p>
						<p class="porudzbina-salon-info">Trajanje iznajmljivanja: {{p.trajanjeNajma}}</p>
						<p class="porudzbina-salon-info">Cena: {{p.cena}} din</p>
			    	</div>
					<div class="porudzbina-lista-vozila">
						<p v-for="vozilo in p.vozila" :key="vozilo.id">{{vozilo.marka}} {{vozilo.model}}</p>
					</div>
					<div class="porudzbina-salon-text">
						<p class="porudzbina-salon-info">Status: {{p.status}}</p>
						<button class="porudzbina-button" v-if="p.status === 'OBRADA'" v-on:click="otkaziPorudzbinu(p)">OTKAZI</button>
						<button class="oceni-button" v-if="p.status === 'VRACENO'" v-on:click="oceni(p)">Oceni</button>
					</div>
					<div class="hidden-layer" v-show="showForm">
						<div class="hidden-layer-container">
							<h2>Ocenite porudzbinu</h2>
							<input type="text" id="komentar" class="form-input-material-porudz" placeholder="Komentar" v-model="komentar">
      						<div class="rating">
            					<span
              						v-for="starClass in starColorClasses"
    								:key="starClass.star"
    								class="star"
    								:class="{ 'filled': starClass.filled, 'hovered': starClass.hovered }"
   									@click="setOcena(starClass.star)"
    								@mouseenter="hoveredStar = starClass.star"
    								@mouseleave="hoveredStar = 0"
            					>
              						&#9733;
            					</span>
          					</div>
          					<input type="submit" value="Oceni" class="btn-porudz" v-on:click="kreirajKomentar()">
      						<button class="hide-form-button" v-on:click="toggleForm()">Zatvori</button>
      					</div>
    				</div>
	    	  	</div>
	    	</div>
	`,
    mounted () {
		
		axios
		.get('rest/korisnici/currentUser')
		.then(response => {
			 this.korisnik = response.data;
			 
			 axios
			 .get('rest/porudzbine/getPorudzbineKorisnik/' + this.korisnik.id)
	         .then(response => {
				this.porudzbine = response.data;
			 });
		});
    },
    methods: {
    	natrag : function() {
			router.push(`/`);
    	}, 
    	otkaziPorudzbinu : function(p){
			if(p.status == "OBRADA") {
				console.log("Otkazano");
				//promeni status
				p.status = "OTKAZANO";
				
				axios
				.put('rest/porudzbine/update', p)
				.then(response => {
					//mozda nest
				});
				
				//skini bodove
				var brojOsvojenihBodova = p.cena/1000 * 133 * 4;
				this.korisnik.bodovi -= brojOsvojenihBodova;
				
				axios
				.put('rest/korisnici/updateBodove', this.korisnik)
				.then(response => {
					//mozda nesto proveriti
				});
			}
		},
		oceni : function(p) {
			this.showForm = true;
      		this.odabranaPorudzbina = p;
      		this.odabranaPorudzbina.ocena = 0;
      		//this.odabranaPorudzbina.tekst = "";
      		this.komentar = "";
		},
		toggleForm : function() {
    		this.showForm = !this.showForm;
  		},
		setOcena : function(ocena) {
      		this.odabranaPorudzbina.ocena = ocena;
    	},
    	kreirajKomentar : function() {
			event.preventDefault();
			//kreiraj komentar
			const komentar = {
				korisnik: { id: this.korisnik.id, korisnickoIme: this.korisnik.korisnickoIme },
				salon: { id: this.odabranaPorudzbina.salon.id },
				tekst: this.komentar,
				ocena: this.odabranaPorudzbina.ocena
			}
			
			if (this.komentar == "" || this.odabranaPorudzbina.ocena === 0) {
				//ne me ze
			}
			else {
				//zahtev za kreiranje komentara
				axios
				.post('rest/komentari/kreiraj', komentar)
				.then(response => {
					//mozda ugasi prozor
					if(response.status === 200) {
						this.showForm = !this.showForm;
					}
				});
			
				console.log("Komentar kreiran");
			}
		}, 
		pretragaPorudzbine: function(){
			var zahtev = this.nazivSalona + '&' + this.pocetniDatum + '&' + this.krajnjiDatum + '&' 
							+ this.cenaOd + '&' + this.cenaDo + '&' + this.selectedSort + '&' + this.korisnik.id;
					
			axios
			.post('rest/porudzbine/pretragaPorudzbine/' + zahtev)
			.then(response => {
				this.porudzbine = response.data;
			});
		}
    },
});