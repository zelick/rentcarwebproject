Vue.component("logovanje-korisnika", { 
	data: function () {
	    return {
			korisnickoIme: null, 
			lozinka: null,
			porukaGreska: null,
			postoji: null,
			ulogovanKorisnik: null
	    }
	},
	template: 
	    `
	    <div>
	    	<ul class="menu-bar">
				<li>
 					<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
 				</li>
			</ul>
		    <form class="form-container">
			    <h2>Logovanje</h2>
			    <div class="form-group">
			      <label for="korisniskoIme">Korisnicko ime:</label>
			      <input type="text" id="korisniskoIme" placeholder="Unesite korisnicko ime" v-model="korisnickoIme" required>
			    </div>
			    <div class="form-group">
			      <label for="lozinka">Lozinka:</label>
			      <input type="password" id="lozinka" placeholder="Unesite lozinku" v-model="lozinka"  required>
			    </div>
			    <div>
			    	<label style="color: red;">{{porukaGreska}}</label>
			    </div>
			    <input type="submit" value="Uloguj se" class="login-button" v-on:click="ulogujSe()">
			    <input type="submit" value="Registruj se" class="login-button" v-on:click="registracija()">
	  	</form>
	   </div>
	    `,
    mounted () {
        this.porukaGreska = '';
        axios
		.post('rest/korisnici/test')
		.then(response => {
			if(response.status === 200) {
				
				console.log("Prosao");
			}
		});
    },
    methods: {
    	ulogujSe : function() {
			if (!this.korisnickoIme || !this.lozinka) {
				this.porukaGreska = 'Unesite korisničko ime i lozinku'; 
			    return;
			}
			
			var korisnik = {
				id: null,
    			korisnickoIme: this.korisnickoIme,
    			lozinka: this.lozinka,
    			ime: null,
			    prezime: null,
    			pol: null,
    			datumRodjenja: null,
    			uloga: null,
    			korpa: null,
    			salon: null,
    			bodovi: null,
    			tipKupca: null
			};
	 		 
	 		axios
	 		.post('rest/korisnici/login', korisnik)
	 		.then(response => {
				 if(response.status === 200) {
					
					router.push(`/`);
				 } else {
					 
					 this.porukaGreska = 'Nevalidno korisničko ime ili lozinka';
				 }
	 		})
	 		.catch(error => {
				 console.error(error);
				 this.porukaGreska = 'Nevalidno korisničko ime ili lozinka';
			});
    	},
    	registracija: function(){
			router.push(`/registracija`)
		},
		natrag : function() {
			router.push(`/`);
    	}
    }
});