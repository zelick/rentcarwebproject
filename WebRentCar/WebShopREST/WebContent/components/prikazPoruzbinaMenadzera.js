Vue.component("prikaz-porudzbina-menadzera", { 
	data: function () {
	    return {
			porudzbine: null, 
			korisnik: null, 
			pocetniDatum: "", 
			krajnjiDatum: "", 
			cenaOd: "", 
			cenaDo: "", 
			selectedSort: "cenaRastuce"
	    }
	},
	    template: `
	    	<div>
	    		<ul class="menu-bar">
					<li>
		 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
		 			</li>
				</ul>
				
				<div style="margin: 60px"></div>
				<h2 class="salon-heading">Pretraga porudzbina</h2>
			    <div style="margin: 20px"></div>
			    
				<div class="holder-pretraga-poruzbina">
					  <div>
					    <label for="start-date-picker">Datum od:</label>
					    <input type="date" id="start-date-picker" v-model="pocetniDatum">
					  </div>
					  
					  <div>
					    <label for="end-date-picker">Datum do:</label>
					    <input type="date" id="end-date-picker" v-model="krajnjiDatum">
				  	  </div>
				  	  
				  	  <div>
				  	  	<label>Cena od:</label>
				  	  	<input type="number" v-model="cenaOd">
				  	  </div>
					  
					  <div>	
					    <label >Cena do:</label>
					    <input type="number" v-model="cenaDo">
				  	  </div>
				  	  
					  <div>
					  	 <label>Sortiraj po:</label>
					  	 <select id="sort" class="porudzbina-sort" v-model="selectedSort">
		                    <option value="cenaRastuce" selected="selected">CENA RASTUCE</option>
		                    <option value="cenaOpadajuce">CENA OPADAJUCE</option>
		                    <option value="datumRastuce">DATUM RASTUCE</option>
		                    <option value="datumOpadajuce">DATUM OPADAJUCE</option>
	                	</select>
					  </div>
					  <div>
					    <button class="login-button" v-on:click="pretragaPorudzbine()">Pretraga</button>
					  </div>
	   			</div>
				
				<div style="margin: 20px"></div>
				<h2 class="salon-heading">Porudzbine</h2>
				<div style="margin: 20px"></div>
				
	    	  <div class="order-container" v-for="p in porudzbine">
					<div class="order-details">
					    <p class="order-name">{{p.salon.naziv}}</p>
						<p class="porudzbina-salon-name"Cena: {{p.cena}}</p>
						<p class="porudzbina-salon-info">Datum iznajmljivanja: {{p.datumIVreme}}</p>
						<p class="porudzbina-salon-info">Trajanje iznajmljivanja: {{p.trajanjeNajma}}</p>
						<p class="porudzbina-salon-info">Korisnik: {{p.kupac.ime}} {{p.kupac.prezime}}</p>
						<p class="porudzbina-salon-info">Ukupna cena: {{p.cena}}</p>
					</div>
					
					<div class="order-vehicle-list">
					    <p class="salon-info">Vozila:</p>
						<p v-for="vozilo in p.vozila">{{vozilo.marka}} {{vozilo.model}}</p>
					</div>
					
					<div class="order-status-div">
						<p>Status: </p>
						<p class="status-info">{{p.status}}</p>
					</div>
					
					<div class="order-actions">
						<button class="login-button" v-on:click="odobriPorudzbinu(p)">ODOBRI</button>
						<button class="login-button" v-on:click="odbijPorudzbinu(p)">ODBIJ</button>
						<button class="login-button" v-on:click="porudzbinaPreuzeta(p)">PREUZETO</button>
						<button class="login-button" v-on:click="porudzbinaVracena(p)">VRACENO</button>
					</div>				
	    	  </div>
	    	</div>
	    `,
    mounted () {
		
		axios
		.get('rest/korisnici/currentUser')
		.then(response => {
			 this.korisnik = response.data;
			 
			 axios.get('rest/porudzbine/getPorudzbineMenadzer/' + this.korisnik.salon.id)
	        .then(response => {
				this.porudzbine = response.data;
			});
		});
    },
    methods: {
    	natrag : function() {
			router.push(`/`);
    	}, 
    	odobriPorudzbinu: function(p){
			if(p.status === "OBRADA"){
				axios.put('rest/porudzbine/odboriPorudzbinu/' + p.id)
				.then(response =>{
					p.status = "ODOBRENO";
				});
			}
		}, 
		odbijPorudzbinu: function(p){
			if(p.status === "OBRADA"){
				axios.put('rest/porudzbine/odbijPorudzbinu/' + p.id)
				.then(response =>{
					p.status = "ODBIJENO";
				});
			}
		}, 
		porudzbinaPreuzeta: function(p){
			const danas = new Date();
			const pocetak = new Date(p.datumIVreme);
			if(danas >= pocetak && p.status === "ODOBRENO"){
				axios.put('rest/porudzbine/preuzmiPorudzbinu/' + p.id)
				.then(response =>{
					p.status = "PREUZETO";
				});
			}
		}, 
		porudzbinaVracena: function(p){
			if(p.status === "PREUZETO"){
				axios.put('rest/porudzbine/vratiPorudzbinu/' + p.id)
				.then(response =>{
					p.status = "VRACENO";
					for(var vozilo of p.vozila){
						axios.put('rest/korisnici/updateStatusDostupno/' + vozilo.id);
						axios.put('rest/saloni/updateStatusDostupno/' + vozilo.id);
					}
				});
			}
		}, 
		pretragaPorudzbine: function(){
			var zahtev = this.pocetniDatum + '&' + this.krajnjiDatum + '&' 
							+ this.cenaOd + '&' + this.cenaDo + '&' + this.selectedSort + '&' + this.korisnik.salon.id;
			axios
			.post('rest/porudzbine/pretragaPorudzbineMenadzer/' + zahtev)
			.then(response => {
				this.porudzbine = response.data;
			});
		}
    }
});