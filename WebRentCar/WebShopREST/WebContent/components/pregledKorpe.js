Vue.component("prikaz-korpa", { 
	data: function () {
	    return {
			korisnik: null, 
			showVozila: true, 
			pocetniDatum: null,
			krajnjiDatum: null, 
			ukupnaCena: null,
			porudzbina: { vozila: [] }
	    }
	},
	    template: `
	    <div>
		<ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="natrag()">Odustani</a>
 			</li>
		</ul>
		
		
		<div style="margin: 60px"></div>
  		<div class="holder-pretraga" v-if="showVozila">
		  <div>
		    <label for="start-date-picker" >Pocetni datum:</label>
		    <input type="date" id="start-date-picker" v-model="pocetniDatum" readonly >
		  </div>
		  <div>
		    <label for="end-date-picker">Krajnji datum:</label>
		    <input type="date" id="end-date-picker" v-model="krajnjiDatum" readonly>
		  </div>
		  <div>
		    <label>Cena: {{ukupnaCena}} </label>
		  </div>
		  <div>
		    <button class="button-dodajUkorpu"  v-on:click="kreirajPorudzbinu()">IZNAJMI</button>
		  </div>
	   </div>
			
		<h1 v-if="!showVozila">Korpa je prazna!</h1>
		
		<div style="margin: 60px"></div>
		<h2 class="salon-heading" v-if="showVozila">Ponuda vozila</h2>
		
		<div class="salon-holder" v-for="v in porudzbina.vozila" v-if="showVozila">
			<div class="salon-image-border">
				<img :src="v.slika" class="salon-image">
			</div>
			<div class="salon-text">
				<p class="salon-name">{{v.marka}} {{v.model}}</p>
				<p class="salon-info">Cena: {{v.cena}}din na dan</p>
				<p class="salon-info">{{v.tip}} {{v.vrsta}}</p>
				<p class="salon-info">{{v.tipGoriva}} {{v.potrosnja}}l/100km</p>
				<p class="salon-info">Opis: {{v.opis}}</p>
				<p class="salon-info">{{slobodanIliNe(v.status)}}</p>
			</div>
		</div>
		
	</div>
	    `,
    mounted () {
	     axios
		.get('rest/korisnici/currentUser')
		.then(response => {
			this.korisnik = response.data
			
			/*if(this.korisnik.korpa.cena == 0){
				this.showVozila = false;
			}else{
				this.showVozila = true;
			}*/
			
			//this.pocetniDatum = this.$route.params.pocetniDatum;
			//this.krajnjiDatum =  this.$route.params.krajnjiDatum;
			
			axios
			.get('rest/porudzbine/privremena/' + this.korisnik.id)
			.then(response => {
				//this.ukupnaCena = response.data;
				this.porudzbina = response.data;
				
				this.pocetniDatum = this.porudzbina.datumIVreme;
				this.krajnjiDatum = this.porudzbina.krajnjiDatum;
				
				this.izracunajUkupnuCenu();
			});
			
	    }); 
    },
    methods: {
    	natrag : function() {
			router.push(`/prikaziObjekat/` + this.porudzbina.salon.id);
    	},
    	slobodanIliNe : function(txt) {
			if(txt === true) {
				return 'Dostupan';
			}
			return 'Zauzet';
		}, 
		kreirajPorudzbinu: function(){
			axios
			.post('rest/porudzbine/kreiraj', this.porudzbina)
			.then(response => {
				if (response.status === 200) {
					
					var brojOsvojenihBodova = this.ukupnaCena/1000 * 133;
					this.korisnik.bodovi += brojOsvojenihBodova;
					
					axios
					.put('rest/korisnici/updateBodove', this.korisnik)
					.then(response => {
						//mozda neka provera
						router.push(`/prikazPorudzbinaKorisnika`); //pregled porudzbina
					});
					
					//router.push(`/`); //prebaciti na pregled porudzbina
				}
			});
		}, 
		izbaciVoziloIzKorpe: function(){
			
		},
		izracunajUkupnuCenu : function() {
			var cena = 0;
			const dani = this.porudzbina.trajanjeNajma;
			
			for(var obj of this.porudzbina.vozila) {
				cena += dani*obj.cena;
			}
			
			this.ukupnaCena = cena * ((100 - this.korisnik.tipKupca.popust)/100);
			this.porudzbina.cena = this.ukupnaCena;
		}
    }
});