Vue.component("menadzer-pregled-salona", { 
	data: function () {
	    return {
			menadzer: null,
			salon: null,
			status: "zatvoren",
			komentari: null
			//obrisiVozilo:null
	    }
	},
	template: `
	<div>
		<ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
 			</li>
		</ul>
		
		<div style="margin: 60px"></div>
		<div class="salon-holder1">
			<div class="salon-image-border">
				<img :src="salon.slika" class="salon-image">
			</div>
			<div class="salon-text">
				<p class="salon-name">{{salon.naziv}}</p>
				<p class="salon-info">{{salon.lokacija.adresa}}</p>
				<p class="salon-info">{{status}}</p>
				<p class="salon-info">Radno vreme: {{salon.radnoVreme}}</p>
				<p class="salon-info">Ocena: {{salon.ocena}}</p>
				<button class="login-button" v-on:click="dodajVozilo()">DODAJ VOZILO</button>
			</div>
		</div>
		
		<h2 class="salon-heading">Vozila</h2>
        <div class="salon-holder1" v-for="s in salon.vozila">
            <div class="salon-image-border">
                <img :src="s.slika" class="salon-image">
            </div>
            <div class="salon-text">
                <p class="salon-name">{{s.marka}} {{s.model}}</p>
                <p class="salon-info">Cena: {{s.cena}}din na dan</p>
                <p class="salon-info">{{s.tip}} {{s.vrsta}}</p>
                <p class="salon-info">{{s.tipGoriva}} {{s.potrosnja}}l/100km</p>
                <p class="salon-info">Opis: {{s.opis}}</p>
                <p class="salon-info">{{slobodanIliNe(s.status)}}</p>
                <button class="oceni-button" v-on:click="IzmeniVozilo(s.id)">IZMENI</button>
                <button class="porudzbina-button" v-on:click="ObrisiVozilo(s.id)">OBRISI</button>
            </div>
        </div>
        
        <h2 class="salon-heading">Komentari</h2>
		<div class="komentar-holder" v-for="k in komentari" v-if="k.odobren">
			<div class="salon-text">
				<p class="salon-name">{{k.korisnik.korisnickoIme}}</p>
				<p class="salon-info">{{k.tekst}}</p>
				<p class="salon-info">Ocena: {{k.ocena}}</p>
				<p class="salon-info">{{daLiJeVidljiv(k)}}</p>
				<div class="komentar-dugmad">
					<button class="oceni-button" v-on:click="odobriKomentar(k)">ODOBRI</button>
					<button class="porudzbina-button" v-on:click="blokirajKomentar(k)">ODBI</button>
				</div>
			</div>
		</div>
	</div>
	`,
    mounted () {
        axios
        .get('rest/korisnici/currentUser')
        .then(response => {
			this.menadzer = response.data;
			
			if(this.menadzer != null) {
				
				axios.get('rest/saloni/id/' + this.menadzer.salon.id)
				.then(response => {
					
					this.salon = response.data;
					
					if (this.salon.status == "true") {
						this.status = "Otvoren";
					}
					
					axios
					.get('rest/komentari/komentariSalonaSvi/' + this.salon.id)
					.then(response => {
						this.komentari = response.data;
					});
				});
			}
		});
    },
    methods: {
    	natrag : function() {
			router.push(`/`);
    	}, 
    	dodajVozilo: function(){
			router.push('/kreirajVozilo/' + this.salon.id);
		}, 
		slobodanIliNe: function(status){
			if(status == 'true') {
				return 'Dostupan';
			}
			return 'Zauzet';
		}, 
		IzmeniVozilo: function(voziloId){
			router.push('/izmenaVozila/' + voziloId);
		},
		ObrisiVozilo: function(voziloId){
			
			axios.delete('rest/korisnici/obrisiVozilo/' + voziloId);
			
			axios.delete('rest/saloni/obrisiVozilo/' + voziloId)
			.then(response =>{
				
				axios.get('rest/saloni/id/' + this.menadzer.salon.id)
				.then(response => {
					this.salon = response.data;
					if (this.salon.status == "true") {
						this.status = "Otvoren";
					}
				});
				
			});
			
			
		},
		odobriKomentar : function(k) {
			//poslati update
			if (k.vidljiv === false) {
				k.vidljiv = true;
				axios
				.put('rest/komentari/update', k)
				.then(response => {
					console.log(k.vidljiv);
				});
				
				var prosecnaOcena = 0;
				var brKomentara = 0;
				
				for(var obj of this.komentari) {
					if (obj.vidljiv === true) {
						prosecnaOcena += obj.ocena;
						brKomentara++;
					}
				}
				if (brKomentara == 0) {
					this.salon.ocena = 0;
					this.menadzer.salon.ocena = 0;
				}
				else {
					const konacnaOcena = prosecnaOcena / brKomentara
					this.salon.ocena = konacnaOcena;
					this.menadzer.salon.ocena = konacnaOcena;
				}
				
				//update ocene objekta
				axios
				.put('rest/saloni/updateOcenu', this.salon)
				.then();			
			
				//update ocenu objekta u menadzeru updateOcenasalona
				axios
				.put('rest/korisnici/updateOcenasalona', this.menadzer)
				.then();
			}
		},
		blokirajKomentar : function(k) {
			if (k.vidljiv === false) {
				k.odobren = false;
				axios
				.put('rest/komentari/update', k)
				.then(response => {
					//
				});
			}
		},
		daLiJeVidljiv : function(k) {
			if(k.vidljiv === false) {
				return "Nije vidljiv";
			}
			else {
				return "Vidljiv";
			}
		}
    }
});