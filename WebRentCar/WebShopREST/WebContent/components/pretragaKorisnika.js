Vue.component("pretraga-Korisnika", { 
	data: function () {
	    return {
			searchKorisnickoIme:  "",
			serachIme:  "",
			serachPrezime:  "",
			selectedSort: "korisnickoImeRastuce", 
			ulogaKupac: false, 
			ulogaMenadzer: false,
			ulogaAdministrator: false
	    }
	},
	    template: `
	   <div>
			<ul class="menu-bar">
					<li>
		 				<a href="#" v-on:click="pocetniEkran()">Pocetni Ekran</a>
		 			</li>
			</ul>
	
		<div style="margin: 60px"></div>
			
		<div class="form-container">
	        <h3>Pretraga</h3>
	        <div>
	            <div class="form-group">
	                <label for="nazivKorisnika">Korisnicko ime:</label>
	                <input type="text" id="nazivKorisnika" class="form-input-material" placeholder="korisnicko ime" v-model="searchKorisnickoIme">
	            </div>
	            <div class="form-group">
	                <label for="ime">Ime:</label>
	                <input type="text" id="ime" class="form-input-material" v-model="serachIme" placeholder="ime">
	            </div>
	            <div class="form-group">
	                <label for="prezime">Prezime:</label>
	                <input type="text" id="prezime" class="form-input-material" v-model="serachPrezime" placeholder="prezime">
	            </div>
	            <div class="form-group">
	                <label for="sort">Soritaj po:</label>
	                <select id="sort" class="form-input-material" v-model="selectedSort">
	                    <option value="korisnickoImeRastuce" selected="selected">KORISNICKO IME RASTUCE</option>
	                    <option value="korisnickoImeOpadajuce">KORISNICKO IME OPADAJUCE</option>
	                    <option value="imeRastuce">IME RASTUCE</option>
	                    <option value="imeOpdajuce">IME OPADAJUCE</option>
	                    <option value="prezimeRastuce">PREZIME RASTUCE</option>
	                    <option value="prezimeOpadajuce">PREZIME OPADAJUCE</option>
	                    <option value="bodoviRastuce">BODOVI RASTUCE</option>
	                    <option value="bodoviOpadajuce">BODOVI OPADAJUCE</option>
	                </select>
	            </div>   
	               <div class="cekbox">
				      <label for="filerVrsta">Uloga:</label>
				      <input type="checkbox" v-model="ulogaKupac" class="custom-checkbox">
				      <label for="filerVrsta" class="checkbox-label">KUPAC</label>
				
				      <input type="checkbox" v-model="ulogaMenadzer" class="custom-checkbox">
				      <label for="filerVrsta" class="checkbox-label">MENADZER</label>
				      
				      <input type="checkbox" v-model="ulogaAdministrator" class="custom-checkbox">
				      <label for="filerVrsta" class="checkbox-label">ADMINISTRATOR</label>
	   			  </div>
	   			  
	            <div class="form-group">
	                <input type="submit" value="Pretrazi" class="btn" v-on:click="pretraga()">
	            </div>
	        </div>
    </div>
    </div>
    `,
    mounted () {
        
    },
    methods: {
    	pocetniEkran : function() {
			router.push(`/`);
    	}, 
    	pretraga: function(){
			if(this.ulogaKupac == true){
				this.ulogaKupac = '1';
			}else{
				this.ulogaKupac = '0';
			}
			
			if(this.ulogaMenadzer == true){
				this.ulogaMenadzer = '1';
			}else{
				this.ulogaMenadzer = '0';
			}
			
			if(this.ulogaAdministrator == true){
				this.ulogaAdministrator = '1';
			}else{
				this.ulogaAdministrator = '0';
			}
			
	
			var zahtev = this.searchKorisnickoIme + '&' +  this.serachIme + '&' + this.serachPrezime 
			+ '&' + this.selectedSort + '&' + this.ulogaKupac + '&' + this.ulogaMenadzer + '&' + this.ulogaAdministrator;
					
			axios
		    .post('rest/korisnici/pretraga/' + zahtev)
		    .then(response => {
				router.push(`/prikazPretrageKorisnika`);
			});
		}
    }
});