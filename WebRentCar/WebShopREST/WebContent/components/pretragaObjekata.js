Vue.component("pretraga-objekata", { 
	data: function () {
	    return {
			searchNaziv: "",
			searchLokacija: "",
			searchTip: "SVE",
			serachOcena: "", 
			selectedSort: "nazivRastuce", 
			vrstaManuleni: false, 
			vrstaAutomatik: false, 
			tipBenzin: false,
			tipDizel: false,
			tipHibrid: false, 
			tipElektricni: false, 
			listaVrsta: null, 
			listaTip: null,
			zahtev: null,
			otvoren: null
	    }
	},
	template: `
	<div>
	<ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
 			</li>
	</ul>
	
	<div style="margin: 60px"></div>
		
	<div class="form-container">
        <h3>Pretraga</h3>
        <div>
            <div class="form-group">
                <label for="nazivObjekta">Naziv objekta:</label>
                <input type="text" id="nazivObjekta" class="form-input-material" placeholder="Naziv" v-model="searchNaziv">
            </div>
            <div class="form-group">
                <label for="lokacijaObjekta">Lokacija:</label>
                <input type="text" id="lokacijaObjekta" class="form-input-material" v-model="searchLokacija" placeholder="Grad ili drzava">
            </div>
            <div class="form-group">
                <label for="tipVozila">Tip vozila:</label>
                <select id="tipVozila" class="form-input-material" v-model="searchTip">
                    <option value="SVE">SVE</option>
                    <option value="AUTO">AUTO</option>
                    <option value="KOMBI">KOMBI</option>
                    <option value="MOBILEHOME">MOBILEHOME</option>
                </select>
            </div>
            <div class="form-group">
                <label for="prosecnaOcena">Prosecna ocena:</label>
                <input type="text" id="prosecnaOcena" class="form-input-material" v-model="serachOcena" placeholder="Ocena">
            </div>
            <div class="form-group">
                <label for="sort">Soritaj po:</label>
                <select id="sort" class="form-input-material" v-model="selectedSort">
                    <option value="nazivRastuce" selected="selected">NAZIV RASTUCE</option>
                    <option value="nazivOpadajuce">NAZIV OPADAJUCE</option>
                    <option value="lokacijaRastuce">LOKACIJA RASTUCE</option>
                    <option value="lokacijaOpadajuce">LOKACIJA OPADAJUCE</option>
                    <option value="ocenaRastuce">OCENA RASTUCE</option>
                    <option value="ocenaOpadajuce">OCENA OPADAJUCE</option>
                </select>
            </div>   
               <div class="cekbox">
			      <label for="filerVrsta">Vrsta vozila:</label>
			      <input type="checkbox" v-model="vrstaManuleni" class="custom-checkbox">
			      <label for="filerVrsta" class="checkbox-label">MANUELNI</label>
			
			      <input type="checkbox" value="AUTOMATIK" v-model="vrstaAutomatik" class="custom-checkbox">
			      <label for="filerVrsta" class="checkbox-label">AUTOMATIK</label>
   			  </div>
            <div class="cekbox">
                <label for="filerTip">Tip goriva:</label>
                <input type="checkbox" v-model="tipBenzin" class="custom-checkbox">
                <label for="filerTip" class="checkbox-label">BENZIN</label>
                <input type="checkbox" v-model="tipDizel" class="custom-checkbox">
                <label for="filerTip" class="checkbox-label">DIZEL</label>
                <input type="checkbox" v-model="tipHibrid" class="custom-checkbox">
                <label for="filerTip" class="checkbox-label">HIBRID</label>
                <input type="checkbox" v-model="tipElektricni" class="custom-checkbox">
                <label for="filerTip" class="checkbox-label">ELEKTRICNI</label>
            </div>
            
            <div class="form-group">
                <div class="cekbox">
                	<label class="cekbox-label">Samo otvoreni objekti</label>
                	<label class="switch">
  						<input type="checkbox" v-model="otvoren">
  						<span class="slider round"></span>
					</label>
				</div>
            </div>
            <div class="form-group">
                <input type="submit" value="Pretrazi" class="btn" v-on:click="pretraga()">
            </div>
        </div>
    </div>
    </div>
    `,
    mounted () {
       
    },
    methods: {
    	pretraga : function() {
			this.listaVrsta = [];
		    if (this.vrstaManuleni == true) {
		      this.listaVrsta.push("MANUELNI");
		    }
		    if (this.vrstaAutomatik == true) {
		      this.listaVrsta.push("AUTOMATIK");
		    }
		    this.listaTip = [];
		    if (this.tipBenzin == true) {
		      this.listaTip.push("BENZIN");
		    }
		    if (this.tipDizel == true) {
		      this.listaTip.push("DIZEL");
		    }
		    if (this.tipHibrid == true) {
		      this.listaTip.push("HIBRID");
		    }
		    if (this.tipElektricni == true) {
		      this.listaTip.push("ELEKTRICINI");
		    }
		    
		    const filter = this.popuniFilter();
		    
		    zahtev = this.searchNaziv + '&' + this.searchTip + '&' + this.searchLokacija + '&' + this.serachOcena + '&' + this.selectedSort + '&' + this.otvoren + '&' + filter;
		    
		    axios
		    .post('rest/saloni/pretraga/' + zahtev)
		    .then(response => {
				router.push(`/prikazPretrageObjekata`);
			})
			.catch(error => {
				console.error(error);
			});
    	},
    	popuniFilter : function() {
			var filter = "";
			if(this.vrstaManuleni == true) {
				filter += '1';
			}
			else {
				filter += '0';
			}
			if(this.vrstaAutomatik == true) {
				filter += '1';
			}
			else {
				filter += '0';
			}
			if(this.tipBenzin == true) {
				filter += '1';
			}
			else {
				filter += '0';
			}
			if(this.tipDizel == true) {
				filter += '1';
			}
			else {
				filter += '0';
			}
			if(this.tipHibrid == true) {
				filter += '1';
			}
			else {
				filter += '0';
			}
			if(this.tipElektricni == true) {
				filter += '1';
			}
			else {
				filter += '0';
			}
			return filter;
		},
    	natrag : function() {
			router.push(`/`);
    	}
    }
});