Vue.component("iznajmljivanje-vozila", { 
	data: function () {
	    return {
			dostupnaVozila: null, 
			pocetniDatum: null, 
			krajnjiDatum: null, 
			showCart: false,
			showCartStyle: 'display: none;', 
			ulogovanKorisnik: null, 
			shouldShowCartItems: false, 
			showVozila: false
	    }
	},
	    template: `
	    <div>
		<ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
 			</li>
		</ul>
		
		<button class="cart-button" v-on:click="toggleCart()">
		  <span class="icon">
		    <img src="https://static.vecteezy.com/system/resources/previews/019/787/018/original/shopping-cart-icon-shopping-basket-on-transparent-background-free-png.png" alt="Shopping Cart Icon" width="40" height="30">
		  </span>
		  <span class="text">Korpa</span>
		</button>
		
		<div class="cart-container" v-bind:style="showCartStyle">
		  <h2 style="margin-bottom: 14px;">Pregled korpe</h2>
		
		
	   <div class="cart-items" v-if="shouldShowCartItems">
          <div class="cart-item" v-for="vozilo in ulogovanKorisnik.korpa.vozila">
            <div class="image-box">
              <img :src="vozilo.slika" style="height: 100px;" />
            </div>
            <div class="about">
              <h1 class="title">{{ vozilo.marka }} {{ vozilo.model }}</h1>
              <h3 class="subtitle">Cena: {{ vozilo.cena }} din na dan</h3>
              <h3 class="subtitle">{{ vozilo.tip }} {{ vozilo.vrsta }}</h3>
            </div>
          </div>
        </div>		
          <p style="margin-top: 8px; margin-bottom: 8px;" v-if="!shouldShowCartItems">Korpa je prazna!</p>
		  <div class="cart-controls">
		    <p style="margin-right: auto;">Cena: {{ ulogovanKorisnik.korpa.cena }}</p>
		    <button class="cart-poruci" v-on:click="pregledKorpe()">Iznajmi</button>
		  </div>
		</div>

  		
  		<div style="margin: 60px"></div>
  		<div class="holder-pretraga">
		  <div>
		    <label for="start-date-picker">Izaberi početni datum:</label>
		    <input type="date" id="start-date-picker" v-model="pocetniDatum">
		  </div>
		  <div>
		    <label for="end-date-picker">Izaberi krajnji datum:</label>
		    <input type="date" id="end-date-picker" v-model="krajnjiDatum">
		  </div>
		  <div>
		    <button class="login-button"  v-on:click="pretragaVozila()">Pretraga</button>
		  </div>
	   </div>

		<div style="margin: 60px"></div>
		<h2 class="salon-heading" v-if="showVozila">Ponuda vozila</h2>
		
		<div class="salon-holder" v-for="v in dostupnaVozila" v-if="showVozila">
			<div class="salon-image-border">
				<img :src="v.slika" class="salon-image">
			</div>
			<div class="salon-text">
				<p class="salon-name">{{v.marka}} {{v.model}}</p>
				<p class="salon-info">Cena: {{v.cena}}din na dan</p>
				<p class="salon-info">{{v.tip}} {{v.vrsta}}</p>
				<p class="salon-info">{{v.tipGoriva}} {{v.potrosnja}}l/100km</p>
				<p class="salon-info">Opis: {{v.opis}}</p>
				<p class="salon-info">{{slobodanIliNe(v.status)}}</p>
			</div>
			<button class="button-dodajUkorpu"  v-on:click="dodajUKorpu(v)">Dodaj u korpu</button>
		</div>
		
	</div>
	    `,
    mounted () {
       // axios.get('rest/korisnici/getSvaVozila')
		//.then(response => {
			//this.dostupnaVozila = response.data; //sta da prikazem na pocetku? sva vozila ili nista?
			
			axios
			.get('rest/korisnici/currentUser')
			.then(response => {
				this.ulogovanKorisnik = response.data
				
				if(this.ulogovanKorisnik.korpa.cena == 0){
					this.shouldShowCartItems = false;
				}else{
					this.shouldShowCartItems = true;
				}
		
		    }); 
		//});
		
	    
    },
    methods: {
    	natrag : function() {
			router.push(`/`);
    	},
    	slobodanIliNe : function(txt) {
			if(txt == 'true') {
				return 'Dostupan';
			}
			return 'Zauzet';
		}, 
		pretragaVozila : function(){
			//zastita za unete datume 
			
			axios.get('rest/korisnici/pretraga/' + this.pocetniDatum + '/' + this.krajnjiDatum)
			.then(response => {
				this.dostupnaVozila = response.data;
				if(this.dostupnaVozila == null || this.dostupnaVozila.length == 0){
					this.showVozila = false;
				}else{
					this.showVozila = true;
				}
			})
			
		},
		toggleCart : function() {
			this.showCart = !this.showCart;
			if(this.showCart) {
				this.showCartStyle = '';
			}
			else {
				this.showCartStyle = 'display: none;';
			}
		},
		dodajUKorpu: function(dodajVozilo){
			axios.post('rest/korisnici/dodajUKorpu/'+ this.ulogovanKorisnik.id, dodajVozilo)
			.then(response => {
				 this.ulogovanKorisnik.korpa = response.data; 
			      const index = this.dostupnaVozila.findIndex(v => v.id === dodajVozilo.id); //izbaci vozilo koje je dodato u korpu
			      if (index !== -1) {
			        this.dostupnaVozila.splice(index, 1);
			      }
			      this.shouldShowCartItems = true;
			});
		}, 
		pregledKorpe : function(){
			//router.push(`/pregledKorpe/${this.pocetniDatum}/${this.krajnjiDatum}`);

			router.push(`/pregledKorpe/` + this.pocetniDatum + `/` + this.krajnjiDatum); //ovde saljes i datume
		}
    }
});