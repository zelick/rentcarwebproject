Vue.component("registracija-korisnika", { 
	data: function () {
	    return {
			korisnik: { korisnickoIme: null, lozinka: null, ime: null, prezime: null, pol: null, datumRodjenja: null },
			proveriLozinku: null, 
			greskaKorisnickoIme: null,
			greskaLozinka: null
	    }
	},
	template: `
	<div class="form-container">
        <h3>Registracija</h3>
        <form>
            <div class="form-group">
                <label for="korisnickoIme">Korisničko ime:</label>
                <input type="text" id="korisnickoIme" class="form-input-material" v-model="korisnik.korisnickoIme" placeholder="Unesite korisničko ime" required>
                <label style="color: red;">{{greskaKorisnickoIme}}</label>
            </div>
            <div class="form-group">
                <label for="lozinka">Lozinka:</label>
                <input type="password" id="lozinka" class="form-input-material" v-model="korisnik.lozinka" placeholder="Unesite lozinku" required>
            </div>
            <div class="form-group">
                <label for="proveriLozinku">Potvrdi lozinku:</label>
                <input type="password" id="proveriLozinku" class="form-input-material" v-model="proveriLozinku" placeholder="Potvrdite lozinku" required>
                <label style="color: red;">{{greskaLozinka}}</label>
            </div>
            <div class="form-group">
                <label for="ime">Ime:</label>
                <input type="text" id="ime" class="form-input-material" v-model="korisnik.ime" placeholder="Unesite ime" required>
            </div>
            <div class="form-group">
                <label for="prezime">Prezime:</label>
                <input type="text" id="prezime" class="form-input-material" v-model="korisnik.prezime" placeholder="Unesite prezime" required>
            </div>
            <div class="form-group">
                <label for="datum">Datum rođenja:</label>
                <input type="date" id="datum" class="form-input-material" v-model="korisnik.datumRodjenja" required>
            </div>
            <div class="form-group">
                <label for="pol">Pol:</label>
                <select id="pol" class="form-input-material" v-model="korisnik.pol" required>
                    <option value="M">MUSKI</option>
                    <option value="Z">ZENSKI</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" value="Registruj se" class="btn"  v-on:click="registracija()">
            </div>
        </form>
    </div>
    `,
    mounted () {
        this.porukaGreska = '';
    },
    methods: {
    	registracija : function() {
			event.preventDefault();
			if(this.korisnik.lozinka != this.proveriLozinku){
				this.greskaLozinka = 'Lozinke nisu jednake!';
				return;
			}else{
				this.greskaLozinka = '';
			}
			axios.get('rest/korisnici/proveriKorisnickoIme/' + this.korisnik.korisnickoIme)
				.then(response =>{
					if(!response.data){
						this.greskaKorisnickoIme = 'Korisnicko ime vec postoji!';
						return;
					}else {
						this.korisnik.uloga = "KUPAC";
						this.greskaKorisnickoIme = ''; 
						axios.post('rest/korisnici/kreiraj', this.korisnik)
						.then(response => {
							router.push(`/logovanje`)
						});
					}
				});			
    	}
    }
});