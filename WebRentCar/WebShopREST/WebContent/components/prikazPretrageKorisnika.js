Vue.component("prikaz-pretrage-korisnika", { 
	data: function () {
	    return {
		   korisiniciDto: null
	    }
	},
	    template: `
	    <div>
	      <ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="pocetniEkran()">Pocetni Ekran</a>
 			</li>
		  </ul>
		  <div class="table-container">
		  <table>
		    <tr>
		      <th>KORISNICKO IME</th>
		      <th>IME</th>
		      <th>PREZIME</th>
		      <th>POL</th>
		      <th>DATUM RODJENJA</th>
		      <th>ULOGA</th>
		      <th>BODOVI</th>
		    </tr>
		    <tr v-for="k in korisiniciDto">
		      <td>{{ k.korisnickoIme }}</td>
		      <td>{{ k.ime }}</td>
		      <td>{{ k.prezime }}</td>
		      <td>{{ k.pol }}</td>
		      <td>{{ k.datumRodjenja }}</td>
		      <td>{{ k.uloga }}</td>
		      <td>{{ k.bodovi }}</td>
		    </tr>
		  </table>
		  </div>
		</div>	    
	    `,
    mounted () {
        axios.get('rest/korisnici/rezulatatPretarageKorisniciDTO')
        .then(response => {
			this.korisiniciDto = response.data;
		});
    },
    methods: {
		pocetniEkran: function(){
			router.push(`/`);
		}, 
    }
});