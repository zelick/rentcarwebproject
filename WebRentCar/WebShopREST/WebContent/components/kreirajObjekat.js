Vue.component("kreiraj-objekat", { 
	data: function () {
	    return {
			      salon: {
			        naziv: null,
			        vozila: [],
			        radnoVreme: null,
			        status: "false",
			        lokacija: { duzina: null, sirina: null, adresa: null },
			        slika: null,
			        ocena: null
			      },
			      menadzeri: null,
			      odabranMenadzer: null,
			      porukaGreska: null
	    }
	},
	    template:
	    `
	    <div>
	    <ul class="menu-bar">
			<li>
 				<a href="#" v-on:click="natrag()">Pocetni Ekran</a>
 			</li>
		</ul>
	    
	    <div class="form-container">
        <h3>Kreiraj salon</h3>
        <form>
            <div class="form-group form-group-inline">
                <label for="nazivObjekta">Naziv:</label>
                <input type="text" id="nazivObjekta" class="form-input-material" v-model="salon.naziv" placeholder="Naziv">
            </div>
            <div class="form-group form-group-inline">
                <label for="adresa">Adresa:</label>
                <input type="text" id="adresa" class="form-input-material" v-model="salon.lokacija.adresa" placeholder="Ulica broj, mesto post broj">
            </div>
            <div class="form-group form-group-inline">
                <label for="geoDuzina">Geo duzina:</label>
                <input type="text" id="geoDuzina" class="form-input-material" v-model="salon.lokacija.duzina" placeholder="Duzina">
            </div>
            <div class="form-group form-group-inline">
                <label for="geoSirina">Geo Sirina:</label>
                <input type="text" id="geoSirina" class="form-input-material" v-model="salon.lokacija.sirina" placeholder="Sirina">
            </div>
            <div class="form-group form-group-inline">
                <label for="radnoVreme">Radno vreme:</label>
                <input type="text" id="radnoVreme" class="form-input-material" v-model="salon.radnoVreme" placeholder="pon-pet">
            </div>
            <div class="form-group form-group-inline">
                <label for="logoSlika">Logo:</label>
                <input type="text" id="logoSlika" class="form-input-material" v-model="salon.slika" placeholder="Slika">
            </div>
            <div class="form-group form-group-inline">
                <label for="menazderSalona">Menadzer:</label>
                <select id="menazderSalona" class="form-input-material" v-model="odabranMenadzer" required>
                    <option v-for="m in menadzeri" :value="m.id">{{m.ime}} {{m.prezime}}({{m.korisnickoIme}})</option>
                </select>
            </div>
            <div>
			    <label style="color: red;">{{porukaGreska}}</label>
			</div>
            <div class="form-group">
                <input type="submit" value="Kreiraj" class="btn" v-on:click="kreiraj()">
            </div>
        </form>
    </div>
    </div>`,
    mounted () {
		axios
		.get('rest/korisnici/slobodniMenadzeri')
		.then(response => {
			this.menadzeri = response.data;
		});
    },
    methods: {
    	kreiraj : function() {
			var validacija = !!(this.salon.naziv && this.salon.radnoVreme && this.salon.lokacija.adresa && this.salon.lokacija.duzina && this.salon.lokacija.sirina && this.salon.slika && this.odabranMenadzer);
			
			if(validacija) {
				
				var adresaReg = this.salon.lokacija.adresa;
				var adresaRegex = /^[a-zA-Z\s]+ \d+,[a-zA-Z\s]+ \d{5}$/;
				
				var duzinaReg = this.salon.lokacija.duzina;
				var sirinaReg = this.salon.lokacija.sirina;
				var cooRegex = /^\d+(\.\d+)?$/;
				
				var link = this.salon.slika;
				var linkRegex = /^https?:\/\/.*\.(?:png|jpe?g|gif)$/i;
				
				var isValidAdresa = adresaRegex.test(adresaReg);
				var isValidDuzina = cooRegex.test(duzinaReg);
				var isValidSirina = cooRegex.test(sirinaReg);
				var isValidLink = linkRegex.test(link);
				
				if(isValidAdresa && isValidDuzina && isValidSirina && isValidLink) {
					axios
					.post('rest/saloni/kreiraj', this.salon)
					.then(response => {
						const salonMenadzera = response.data;
				
						axios
						.post('rest/korisnici/dodajSalon/' + this.odabranMenadzer, salonMenadzera)
						.then(response => {
					
							router.push(`/`);
						});
					});	
				}
				else {
					this.porukaGreska = "Formati nisu ispostovani.";
				}
			}
			else {
				this.porukaGreska = "Sva polja moraju biti popunjena.";
			}	
    	},
    	natrag : function() {
			router.push(`/`);
    	}
    }
});