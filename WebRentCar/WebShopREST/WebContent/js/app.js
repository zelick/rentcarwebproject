const Logovanje = { template: '<logovanje-korisnika></logovanje-korisnika>' }
const Registracija = { template: '<registracija-korisnika></registracija-korisnika>' }
const PocetniEkran = { template: '<pocetni-ekran></pocetni-ekran>' }
const PrikazIzmenaProfila = { template: '<prikazProfila-korisnika></prikazProfila-korisnika>' }
const PrikazObjekta = { template: '<prikaz-objekta></prikaz-objekta>' }
const KreirajObjekat = { template: '<kreiraj-objekat></kreiraj-objekat>' }
const PretragaObjekata = { template: '<pretraga-objekata></pretraga-objekata>' }
const PrikazPretrageObjekata = { template: '<prikaz-pretrage-objekata></prikaz-pretrage-objekata>' }
const RegistracijaMenadzera = { template: '<registracija-menadzera></registracija-menadzera>' }
const MenadzerPregledObjekta = { template: '<menadzer-pregled-salona></menadzer-pregled-salona>' }
const KreirajVozilo = { template: '<kreiraj-vozilo></kreiraj-vozilo>' }
const IzmenaVozila = { template: '<izmeni-vozilo></izmeni-vozilo>' }
const PrikazKorisnikaAdministratoru = { template: '<prikaz-svih-korisnika></prikaz-svih-korisnika>' }
const PretragaKorisnika = { template: '<pretraga-Korisnika></pretraga-Korisnika>' }
const PrikazPretrageKorisnika = { template: '<prikaz-pretrage-korisnika></prikaz-pretrage-korisnika>'}
const IznajmljivanjeVozila = { template: '<iznajmljivanje-vozila></iznajmljivanje-vozila>'}
const PregledKorpe = { template: '<prikaz-korpa></prikaz-korpa">'}
const PrikazPorudzbinaKorisnika = { template: '<prikaz-porudzbina></prikaz-porudzbina>'}
const PrikazPorudzbinaMenadzera = { template: '<prikaz-porudzbina-menadzera></prikaz-porudzbina-menadzera>'}

const router = new VueRouter({
	mode: 'hash',
	routes: [
		{ path: '/', name: 'home', component: PocetniEkran },
		{ path: '/logovanje', component: Logovanje },
		{ path: '/registracija', component: Registracija },
		{ path: '/prikazProfila', component: PrikazIzmenaProfila },
		{ path: '/prikaziObjekat/:id', component: PrikazObjekta },
		{ path: '/kreirajObjekat', component: KreirajObjekat },
		{ path: '/pretragaObjekata', component: PretragaObjekata },
		{ path: '/prikazPretrageObjekata', component: PrikazPretrageObjekata },
		{ path: '/registracijaMenadzera', component: RegistracijaMenadzera },
		{ path: '/pregledObjekta', component: MenadzerPregledObjekta },
		{ path: '/kreirajVozilo/:idObjekta', component: KreirajVozilo },
		{ path: '/izmenaVozila/:idVozila', component: IzmenaVozila },
		{ path: '/prikaziSveKorsinike', component: PrikazKorisnikaAdministratoru},
		{ path: '/pretragaKorisnika', component: PretragaKorisnika},
		{ path: '/prikazPretrageKorisnika', component: PrikazPretrageKorisnika},
		{ path: '/iznajmljivanjeVozila', component: IznajmljivanjeVozila},
		{ path: '/pregledKorpe', component: PregledKorpe},
		{ path: '/prikazPorudzbinaKorisnika', component: PrikazPorudzbinaKorisnika},
		{ path: '/prikazPorudzbinaMenadzera', component: PrikazPorudzbinaMenadzera}
		
	  ]
});

var app = new Vue({
	router,
	el: '#element'
});