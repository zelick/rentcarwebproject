package dto;

import java.util.ArrayList;

import enums.StatusPorudzbine;
import model.Korisnik;
import model.Salon;
import model.Vozilo;

public class PorudzbinaDto {
	private int id;
	private ArrayList<Vozilo> vozila;
	private Salon salon;
	private String datumIVreme;
	private String krajnjiDatum;
	private String trajanjeNajma;
	private double cena;
	private KorisnikDto kupac;
	private StatusPorudzbine status;


	public PorudzbinaDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PorudzbinaDto(ArrayList<Vozilo> vozila, Salon salon, String datumIVreme, String krajnjiDatum,
			String trajanjeNajma, double cena, KorisnikDto kupac) {
		super();
		this.vozila = vozila;
		this.salon = salon;
		this.datumIVreme = datumIVreme;
		this.krajnjiDatum = krajnjiDatum;
		this.trajanjeNajma = trajanjeNajma;
		this.cena = cena;
		this.kupac = kupac;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKrajnjiDatum() {
		return krajnjiDatum;
	}

	public void setKrajnjiDatum(String krajnjiDatum) {
		this.krajnjiDatum = krajnjiDatum;
	}

	public ArrayList<Vozilo> getVozila() {
		return vozila;
	}

	public void setVozila(ArrayList<Vozilo> vozila) {
		this.vozila = vozila;
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

	public String getDatumIVreme() {
		return datumIVreme;
	}

	public void setDatumIVreme(String datumIVreme) {
		this.datumIVreme = datumIVreme;
	}

	public String getTrajanjeNajma() {
		return trajanjeNajma;
	}

	public void setTrajanjeNajma(String trajanjeNajma) {
		this.trajanjeNajma = trajanjeNajma;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public KorisnikDto getKupac() {
		return kupac;
	}

	public void setKupac(KorisnikDto kupac) {
		this.kupac = kupac;
	}
	public StatusPorudzbine getStatus() {
		return status;
	}

	public void setStatus(StatusPorudzbine status) {
		this.status = status;
	}
	
}
