package dto;

import java.util.ArrayList;

import model.Lokacija;
import model.Vozilo;

public class SalonDto {
	private String id;
	private String naziv;
	private ArrayList<Vozilo> vozila;
	private String radnoVreme; //ovo proveri
	private String status; //true-radi
	private Lokacija lokacija;
	private String slika; 
	private String ocena;
	
	public SalonDto() {
		super();
	}

	public SalonDto(String id, String naziv, ArrayList<Vozilo> vozila, String radnoVreme, String status,
			Lokacija lokacija, String slika, String ocena) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.vozila = vozila;
		this.radnoVreme = radnoVreme;
		this.status = status;
		this.lokacija = lokacija;
		this.slika = slika;
		this.ocena = ocena;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<Vozilo> getVozila() {
		return vozila;
	}

	public void setVozila(ArrayList<Vozilo> vozila) {
		this.vozila = vozila;
	}

	public String getRadnoVreme() {
		return radnoVreme;
	}

	public void setRadnoVreme(String radnoVreme) {
		this.radnoVreme = radnoVreme;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Lokacija getLokacija() {
		return lokacija;
	}

	public void setLokacija(Lokacija lokacija) {
		this.lokacija = lokacija;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public String getOcena() {
		return ocena;
	}

	public void setOcena(String ocena) {
		this.ocena = ocena;
	}
}
