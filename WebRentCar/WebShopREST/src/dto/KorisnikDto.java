package dto;

import java.util.ArrayList;

import model.Korpa;
import model.Porudzbina;
import model.Salon;
import model.TipKupca;

public class KorisnikDto {
	private String id;
	private String korisnickoIme;
	private String lozinka;
	private String ime;
	private String prezime;
	private String pol;
	private String datumRodjenja;
	private String uloga;
	private Korpa korpa;
	private Salon salon;
	private double bodovi;
	private TipKupca tipKupca;
	private boolean blokiran;
	//private ArrayList<Porudzbina> svePorudzbine;
	
	public KorisnikDto() {
		super();
		this.blokiran = false;
	}
	

	public KorisnikDto(String id, String korisnickoIme, String lozinka, String ime, String prezime, String pol,
			String datumRodjenja, String uloga, Korpa korpa, Salon salon,
			double bodovi, TipKupca tipKupca,boolean blokiran) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		this.pol = pol;
		this.datumRodjenja = datumRodjenja;
		this.uloga = uloga;
		this.korpa = korpa;
		this.salon = salon;
		this.bodovi = bodovi;
		this.tipKupca = tipKupca;
		this.blokiran = blokiran;
		//this.svePorudzbine = svePorudzbine;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(String datum) {
		this.datumRodjenja = datum;
	}

	public String getUloga() {
		return uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

	public double getBodovi() {
		return bodovi;
	}

	public void setBodovi(double bodovi) {
		this.bodovi = bodovi;
	}

	public TipKupca getTipKupca() {
		return tipKupca;
	}

	public void setTipKupca(TipKupca tipKupca) {
		this.tipKupca = tipKupca;
	}
	
	public boolean isBlokiran() {
		return blokiran;
	}

	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}

	/*public ArrayList<Porudzbina> getSvePorudzbine() {
		return svePorudzbine;
	}

	public void setSvePorudzbine(ArrayList<Porudzbina> svePorudzbine) {
		this.svePorudzbine = svePorudzbine;
	}*/
}
