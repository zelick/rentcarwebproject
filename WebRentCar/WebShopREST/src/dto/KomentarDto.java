package dto;

import model.Salon;

public class KomentarDto {
	private int id;
	private KorisnikDto korisnik;
	private Salon salon;
	private String tekst;
	private int ocena; // od 1 do 5
	private boolean vidljiv;
	private boolean odobren;
	
	public KomentarDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KomentarDto(int id, KorisnikDto korisnik, Salon salon, String tekst, int ocena, boolean vidljiv,
			boolean odobren) {
		super();
		this.id = id;
		this.korisnik = korisnik;
		this.salon = salon;
		this.tekst = tekst;
		this.ocena = ocena;
		this.vidljiv = vidljiv;
		this.odobren = odobren;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public KorisnikDto getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(KorisnikDto korisnik) {
		this.korisnik = korisnik;
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	public boolean isVidljiv() {
		return vidljiv;
	}

	public void setVidljiv(boolean vidljiv) {
		this.vidljiv = vidljiv;
	}

	public boolean isOdobren() {
		return odobren;
	}

	public void setOdobren(boolean odobren) {
		this.odobren = odobren;
	}
}
