package services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import dao.KorisnikDao;
import model.Porudzbina;
import dao.PorudzbinaDao;
import dto.KorisnikDto;
import dto.PorudzbinaDto;

@Path("/porudzbine")
public class PorudzbinaService {
	
	@Context
	ServletContext ctx;
	
	public PorudzbinaService() {
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("porudzbinaDao") == null) {
			String relativePath = "/data/porudzbine.json";
	    	String absolutePath = ctx.getRealPath(relativePath);
			ctx.setAttribute("porudzbinaDao", new PorudzbinaDao(absolutePath));
		}
		
	}
	
	@POST
	@Path("/kreiraj")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PorudzbinaDto newPorudzbina(PorudzbinaDto porudzbina) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.create(porudzbina);
	}
	
	@POST
	@Path("/kreirajPrivremeno")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PorudzbinaDto privremenaPorudzbina(PorudzbinaDto porudzbina) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.createPrivremeno(porudzbina);
	}
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Porudzbina updateKorisnik(PorudzbinaDto porudzbina) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.update(porudzbina);
	}
	
	@GET
	@Path("/privremena/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public PorudzbinaDto getprivremenaPorudzbina(@PathParam("id") String id) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.getprivremenaPorudzbina(Integer.parseInt(id));
	}
	
	
	@GET
	@Path("/kupac/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Porudzbina> getPorudzbineKupca(@PathParam("id") String id) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.getPorudzbineByKupac(Integer.parseInt(id));
	}
	
	@GET
	@Path("/menadzer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Porudzbina> getPorudzbineMenadzera(@PathParam("id") String id) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.getPorudzbineByMenadzer(Integer.parseInt(id));
	}
	
	@GET
	@Path("/dostupnostVozila/{zahtev}")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean getDostupnostVozila(@PathParam("zahtev") String zahtev) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		String[] z = zahtev.split("&");
		return dao.isSlobodan(z[0], z[1], Integer.parseInt(z[2]));
	}
	
	@GET
	@Path("/getPorudzbineKorisnik/{idKorisnika}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<PorudzbinaDto> getPorudzbineDtoByKupac(@PathParam("idKorisnika") int idKorisnika) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.getPorudzbineDtoByKupac(idKorisnika);
	}
	
	@GET
	@Path("/getPorudzbineMenadzer/{idSalona}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<PorudzbinaDto> getPorudzbineDtoByMenadzer(@PathParam("idSalona") int idSalona) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.getPorudzbineDtoByMenadzer(idSalona);
	}
	
	@PUT
	@Path("/odboriPorudzbinu/{idPorudzbine}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Porudzbina prihavtiPorudzbinu(@PathParam ("idPorudzbine") int idPorudzbine) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.prihvatiPoruzbinu(idPorudzbine);
	}

	@PUT
	@Path("/odbijPorudzbinu/{idPorudzbine}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Porudzbina odbijPorudzbinu(@PathParam ("idPorudzbine") int idPorudzbine) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.odbijPoruzbinu(idPorudzbine);
	}
	
	@PUT
	@Path("/preuzmiPorudzbinu/{idPorudzbine}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Porudzbina preuzmiPorudzbinu(@PathParam ("idPorudzbine") int idPorudzbine) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.preuzmiPorudzbinu(idPorudzbine);
	}
	
	@PUT
	@Path("/vratiPorudzbinu/{idPorudzbine}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Porudzbina vratiPorudzbinu(@PathParam ("idPorudzbine") int idPorudzbine) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.vratiPorudzbinu(idPorudzbine);
	}
	
	@POST
	@Path("/pretragaPorudzbine/{zahtev}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<PorudzbinaDto> pretragaPorudzbinaKorisnik(@PathParam ("zahtev") String zahtev) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		String[] z = zahtev.split("&");
		return dao.pretragaPorudzbineKorisnik(z[0], z[1], z[2], z[3], z[4], z[5], Integer.parseInt(z[6]));
	}
	
	@POST
	@Path("/pretragaPorudzbineMenadzer/{zahtev}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<PorudzbinaDto> pretragaPorudzbineMenadzer(@PathParam ("zahtev") String zahtev) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		String[] z = zahtev.split("&");
		return dao.pretragaPorudzbineMenadzer(z[0], z[1], z[2], z[3], z[4], Integer.parseInt(z[5]));
	}
	
	
	@GET
	@Path("/daLiJeSumnjiv/{idKupca}")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean isKupacSumnjiv(@PathParam("idKupca") int id) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.isSumljiv(id);
	}
	
	//axios.post('rest/porudzbine/nadjiSveSumnjive', this.korisiniciDto)
	/*@POST
	@Path("/nadjiSveSumnjive")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<KorisnikDto> nadjiSumnjive(ArrayList<KorisnikDto> korisniciDto) {
		PorudzbinaDao dao = (PorudzbinaDao) ctx.getAttribute("porudzbinaDao");
		return dao.nadjiSumnjive(korisniciDto);
	}*/
}
