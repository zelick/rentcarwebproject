package services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import dao.KorisnikDao;
import dao.SalonDao;
import dto.SalonDto;
import model.Korisnik;
import model.Salon;
import model.Vozilo;

@Path("/saloni")
public class SalonService {
	
	@Context
	ServletContext ctx;
	
	public SalonService() {
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("salonDao") == null) {
			String relativePath = "/data/saloni.json";
			//String relativePath = "";
	    	String absolutePath = ctx.getRealPath(relativePath);
			ctx.setAttribute("salonDao", new SalonDao(absolutePath));
		}
	}
	
	@POST 
	@Path("/kreiraj")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Salon kreirajSalon(SalonDto salonDto){
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.create(salonDto);
	}
	
	@POST 
	@Path("/izmeniVozilo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Salon izmeniVozilo(Vozilo vozilo){
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.izmeniVoziloSalona(vozilo);
	}
	
	@POST 
	@Path("/dodajVozilo/{idSalona}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public int dodajVozilo(@PathParam("idSalona")int idSalona, Vozilo vozilo){
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.dodajVozilo(idSalona, vozilo);
	}
	
	@DELETE
	@Path("/obrisiVozilo/{idVozilo}")
	@Produces(MediaType.APPLICATION_JSON)
	public Vozilo deleteVoziloSalona(@PathParam ("idVozilo") int voziloId) {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.deleteVozilo(voziloId);
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Salon> getSaloni() {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.getAll();
	}
	
	@GET
	@Path("/getVoziloById/{voziloId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Vozilo getVoziloById(@PathParam ("voziloId") int voziloId) {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.getVoziloById(voziloId);
	}
	
	@GET
	@Path("/pretraga")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Salon> getPretragaSaloni() {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.getPretraga();
	}
	
	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public SalonDto GetSalon(@PathParam("id") String id) {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.getSalonById(Integer.parseInt(id));
	}
	
	@POST
	@Path("/pretraga/{zahtev}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void pretrazi(@PathParam("zahtev") String zahtev) {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		String[] s = zahtev.split("&");
		dao.pretrazi(s[0], s[1], s[2], s[3], s[4], s[6], s[5]);
	}

	@GET
	@Path("/dostupnaVozila/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Vozilo> GetDostupnaVozila(@PathParam("id") String id) {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.getDostupnaVozila(Integer.parseInt(id));
	}
	
	@PUT
	@Path("/updateStatusDostupno/{idVozila}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Salon upateStatusDostupno(@PathParam ("idVozila") int idVozila) {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.upateStatusDostupno(idVozila);
	}
	
	@PUT
	@Path("/updateOcenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Salon upateStatusDostupno(Salon salon) {
		SalonDao dao = (SalonDao)ctx.getAttribute("salonDao");
		return dao.updateOcenu(salon);
	}
}
