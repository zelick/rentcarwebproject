package services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDao;
import dao.SalonDao;
import dto.KorisnikDto;
import model.Korisnik;
import model.Korpa;
import model.Porudzbina;
import model.Salon;
import model.Vozilo;

@Path("/korisnici")
public class KorisnikService {
	
	@Context
	ServletContext ctx;
	
	public KorisnikService() {
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("korisnikDao") == null) {
			String relativePath = "/data/korisnici.json";
			//String relativePath = "";
	    	String absolutePath = ctx.getRealPath(relativePath);
			ctx.setAttribute("korisnikDao", new KorisnikDao(absolutePath));
		}
		
	}
	
	private Response addCorsHeaders(Response.ResponseBuilder responseBuilder) {
	    responseBuilder.header("Access-Control-Allow-Origin", "*")
	                   .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
	                   .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	    return responseBuilder.build();
	}
	
	
	@POST
	@Path("/test")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response test() {
		return addCorsHeaders(Response.status(200));
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(KorisnikDto korisnikDto, @Context HttpServletRequest request) {
	    KorisnikDao userDao = (KorisnikDao) ctx.getAttribute("korisnikDao");
	    KorisnikDto loggedUser = userDao.find(korisnikDto.getKorisnickoIme(), korisnikDto.getLozinka());
	    if (loggedUser == null) {
	        return addCorsHeaders(Response.status(400).entity("Invalid username and/or password"));
	    }
	    request.getSession().setAttribute("user", loggedUser);
	    return addCorsHeaders(Response.status(200));
	}
	
	@GET
	@Path("/currentUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public KorisnikDto login(@Context HttpServletRequest request) {
		return (KorisnikDto) request.getSession().getAttribute("user");
	}
	
	@POST
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void logout(@Context HttpServletRequest request) {
		request.getSession().invalidate();
	}
	
	@GET
	@Path("/svikorisniciDTO")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<KorisnikDto> getKorisniciDTO() {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.getAllDTO();
	}
	
	
	@GET
	@Path("/svikorisnici")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Korisnik> getKorisnici() {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.getAll();
	}
	
	@POST
	@Path("/dodajVozilo/{korisnikId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Korisnik login(@PathParam ("korisnikId") int korisnikId, Vozilo vozilo) {
	    KorisnikDao userDao = (KorisnikDao) ctx.getAttribute("korisnikDao");
	    return userDao.dodajVozilo(korisnikId, vozilo);
	}
		
	@POST
	@Path("/izmeniVozilo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Korisnik login(Vozilo vozilo) {
	    KorisnikDao userDao = (KorisnikDao) ctx.getAttribute("korisnikDao");
	    return userDao.izmeniVoziloKorisniku(vozilo);
	}	
	
	@DELETE
	@Path("/obrisiVozilo/{idVozilo}")
	@Produces(MediaType.APPLICATION_JSON)
	public Vozilo deleteVoziloSalona(@PathParam ("idVozilo") int voziloId) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.deleteVozilo(voziloId);
	}
	
	
	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public KorisnikDto getKorisnikById(@PathParam("id") String idx) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.getById(Integer.parseInt(idx));
	}
	
	@GET
	@Path("/username/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Korisnik getKorisnikByUserame(@PathParam("name") String userName) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.getByUserName(userName);
	}
	
	@GET
	@Path("/username/{korisnickoIme}/password/{lozinka}")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean findUsernameAndPassword(@PathParam("korisnickoIme") String korisnickoIme, @PathParam("lozinka") String lozinka) {
	    KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
	    return dao.isUsernameAndPasswordExists(korisnickoIme, lozinka);
	}
	
	@GET
	@Path("/getLoggesUser")
	@Produces(MediaType.APPLICATION_JSON)
	public int findUsernameAndPassword() {
	    KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
	    return dao.getLoggedUser();
	}
	
	@GET
	@Path("/proveriKorisnickoIme/{korisnickoIme}")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean isUsernameExists(@PathParam("korisnickoIme")String ime) {
	    KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
	    return dao.isUsernameExists(ime);
	}
	
	@GET
	@Path("/slobodniMenadzeri/")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<KorisnikDto> getSlobodneMenazdere() {
	    KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
	    return dao.getSlobodneMenadzere();
	}
	
	@GET
	@Path("/proveriDostupnostMenadzera/")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean isMenadzeriSlobodni() {
	    KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
	    return dao.isMenadzerSlobodan();
	}
	
	@POST
	@Path("/dodajSalon/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KorisnikDto dodajSalon(@PathParam("id") String id, Salon salon) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.dodajSalonMenadzeru(Integer.parseInt(id), salon);
	}
	
	@POST
	@Path("/kreiraj")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Korisnik newKorisnik(KorisnikDto korisnik) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.create(korisnik);
	}

	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KorisnikDto updateKorisnik(KorisnikDto korisnik, @Context HttpServletRequest request) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		
		KorisnikDto dto = dao.update(korisnik);
		
		request.getSession().setAttribute("user", dto);
		
		return (KorisnikDto) request.getSession().getAttribute("user");
	}
	
	@PUT
	@Path("/updateBodove")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Korisnik updateKorisnikBodovi(KorisnikDto korisnik) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.updateBodovi(korisnik);
	}
	
	@DELETE
	@Path("/izbrisi")
	@Produces(MediaType.APPLICATION_JSON)
	public Korisnik deleteKorisnik(Korisnik korisnik) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.delete(korisnik);
	}
	
	@POST
	@Path("/pretraga/{zahtev}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void pretrazi(@PathParam("zahtev") String zahtev) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		String[] s = zahtev.split("&");
		dao.pretrazi(s[0], s[1], s[2], s[3], s[4], s[5], s[6]);
	}

	@GET
	@Path("/rezulatatPretarageKorisniciDTO")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<KorisnikDto> getRezultatPretarge() {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.getPretaragaKorisniciDTO();
	}
	
	//da li ovde treba post?
	/*@GET
	@Path("/pretraga/{pocetniDatium}/{krajnjiDatum}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Vozilo> pretragVozila(@PathParam("pocetniDatium") String pocetniDatium, @PathParam("krajnjiDatum") String krajnjiDatum) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.pretragaVozila(pocetniDatium, krajnjiDatum);
	}*/
	
	@GET
	@Path("/getSvaVozila")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Vozilo> getSvaVozila() {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.getSvaVozila();
	}
	
	@POST
	@Path("/dodajUKorpu/{idKorisnika}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Korpa dodajVoziloUKorpu(@PathParam ("idKorisnika") int idKorisnika, Vozilo vozilo) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.dodajVoziloUKorpu(idKorisnika, vozilo);
	}
	
	@GET
	@Path("/getKorpaKorisnik/{idKorisnika}")
	@Produces(MediaType.APPLICATION_JSON)
	public Korpa getKorpaKorisnika(@PathParam ("idKorisnika") int idKorisnika) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.getKorpaKorisnika(idKorisnika);
	}
	
	@POST
	@Path("/isprazniKorpu/{korisnikId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Korpa dodajVoziloUKorpu(@PathParam ("korisnikId") int korisnikId) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.isprazniKorpu(korisnikId);
	}
	
	@POST
	@Path("/blokirajKorisnika/{korisnikId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Korisnik blokirajKorisnika(@PathParam ("korisnikId") int korisnikId) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.blokirajKorisnika(korisnikId);
	}
	
	
	@GET
	@Path("/izracunajCenuPorudzbine/{pocetniDatum}/{krajnjiDatum}/{idKorisnik}")
	@Produces(MediaType.APPLICATION_JSON)
	public double izracunajCenuPorudzbine(@PathParam("pocetniDatum") String pocetniDatum,
			@PathParam("krajnjiDatum") String krajnjiDatum,@PathParam ("idKorisnik") int idKorisnika) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.izracunajCenuPorudzbine(pocetniDatum, krajnjiDatum, idKorisnika);
	}
	

	@PUT
	@Path("/updateStatusDostupno/{idVozila}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Korisnik upateStatusDostupno(@PathParam ("idVozila") int idVozila) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.upateStatusDostupno(idVozila);
	}
	
	@PUT
	@Path("/updateOcenasalona")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KorisnikDto upateStatusDostupno(KorisnikDto korisnik) {
		KorisnikDao dao = (KorisnikDao) ctx.getAttribute("korisnikDao");
		return dao.updateOcenuObjekta(korisnik);
	}
}
