package services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import dao.KomentarDao;
import dao.KorisnikDao;
import dto.KomentarDto;
import dto.KorisnikDto;
import model.Komentar;
import model.Korisnik;

@Path("/komentari")
public class KomentarService {
	
	@Context
	ServletContext ctx;
	
	public KomentarService() {
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("komentarDao") == null) {
			String relativePath = "/data/komentari.json";
	    	String absolutePath = ctx.getRealPath(relativePath);
			ctx.setAttribute("komentarDao", new KomentarDao(absolutePath));
		}
	}
	
	@GET
	@Path("/komentariSalonaSvi/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Komentar> getKomentareKorisnik(@PathParam("id") String id) {
		KomentarDao dao = (KomentarDao) ctx.getAttribute("komentarDao");
		return dao.getSve(Integer.parseInt(id));
	}
	
	@GET
	@Path("/komentariSalonaVidljivi/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Komentar> getKomentareSalon(@PathParam("id") String id) {
		KomentarDao dao = (KomentarDao) ctx.getAttribute("komentarDao");
		return dao.getVidljive(Integer.parseInt(id));
	}
	
	@POST
	@Path("/kreiraj")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KomentarDto newKomentar(KomentarDto komentar) {
		KomentarDao dao = (KomentarDao) ctx.getAttribute("komentarDao");
		return dao.create(komentar);
	}
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KomentarDto updateKomentar(KomentarDto komentar) {
		KomentarDao dao = (KomentarDao) ctx.getAttribute("komentarDao");
		return dao.update(komentar);
	}
}
