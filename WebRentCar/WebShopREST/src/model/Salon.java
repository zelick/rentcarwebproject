package model;

import java.util.ArrayList;

public class Salon {
	private int id;
	private String naziv;
	private ArrayList<Vozilo> vozila;
	private String radnoVreme; //ovo proveri
	private boolean status; //true-radi
	private Lokacija lokacija;
	private String slika;
	private double ocena;
	
	public Salon() {
		super();
		vozila = new ArrayList<Vozilo>();
	}

	public Salon(int id, String naziv, ArrayList<Vozilo> vozila, String radnoVreme, boolean status, Lokacija lokacija, String slika, double ocena) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.vozila = vozila;
		this.radnoVreme = radnoVreme;
		this.status = status;
		this.lokacija = lokacija;
		this.slika = slika;
		this.ocena = ocena;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<Vozilo> getVozila() {
		return vozila;
	}

	public void setVozila(ArrayList<Vozilo> vozila) {
		this.vozila = vozila;
	}

	public String getRadnoVreme() {
		return radnoVreme;
	}

	public void setRadnoVreme(String radnoVreme) {
		this.radnoVreme = radnoVreme;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Lokacija getLokacija() {
		return lokacija;
	}

	public void setLokacija(Lokacija lokacija) {
		this.lokacija = lokacija;
	}

	public double getOcena() {
		return ocena;
	}

	public void setOcena(double ocena) {
		this.ocena = ocena;
	}
}
