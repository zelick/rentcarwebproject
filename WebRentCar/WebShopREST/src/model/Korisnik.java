package model;

import java.time.LocalDate;
import java.util.ArrayList;

import enums.Pol;
import enums.Uloga;

public class Korisnik {
	private int id;
	private String korisnickoIme;
	private String lozinka;
	private String ime;
	private String prezime;
	private Pol pol;
	private LocalDate datumRodjenja;
	private Uloga uloga;
	//private ArrayList<Porudzbina> svePorudzbine;
	private Korpa korpa;
	private Salon salon;
	private double bodovi;
	private TipKupca tipKupca;
	private boolean blokiran;


	public Korisnik() {
		super();
		//this.svePorudzbine = new ArrayList<Porudzbina>();
		this.korpa = new Korpa();
		this.salon = new Salon();
		this.tipKupca = new TipKupca();
		this.bodovi = 0;
		this.blokiran = false;
	}

	public Korisnik(int id, String korisnickoIme, String lozinka, String ime, String prezime, Pol pol, LocalDate datumRodjenja,
			Uloga uloga, Korpa korpa, Salon salon, double bodovi, TipKupca tipKupca, boolean blokiran) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		this.pol = pol;
		this.datumRodjenja = datumRodjenja;
		this.uloga = uloga;
		//this.svePorudzbine = svePorudzbine;
		this.korpa = korpa;
		this.salon = salon;
		this.bodovi = bodovi; 
		this.tipKupca = tipKupca;
		this.blokiran = blokiran;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Pol getPol() {
		return pol;
	}

	public void setPol(Pol pol) {
		this.pol = pol;
	}

	public LocalDate getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(LocalDate datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	public double getBodovi() {
		return bodovi;
	}

	public void setBodovi(double bodovi) {
		this.bodovi = bodovi;
	}

	public TipKupca getTipKupca() {
		return tipKupca;
	}

	public void setTipKupca(TipKupca tipKupca) {
		this.tipKupca = tipKupca;
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}
	
	public boolean isBlokiran() {
		return blokiran;
	}

	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}

	/*public ArrayList<Porudzbina> getSvePorudzbine() {
		return svePorudzbine;
	}

	public void setSvePorudzbine(ArrayList<Porudzbina> svePorudzbine) {
		this.svePorudzbine = svePorudzbine;
	}*/
}
