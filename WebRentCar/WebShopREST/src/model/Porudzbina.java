package model;

import java.time.LocalDate;
import java.util.ArrayList;

import enums.StatusPorudzbine;

public class Porudzbina {
	private int id; // trebalo bi mozda pretvoriti u string
	private ArrayList<Vozilo> vozila;
	private Salon salon;
	private LocalDate datumIVreme;
	private String trajanjeNajma; // mozda neki datum format
	private double cena;
	private Korisnik kupac;
	private StatusPorudzbine status;
	
	public Porudzbina() {
		super();
		/*vozila = new ArrayList<Vozilo>();
		salon = new Salon();
		kupac = new Korisnik();*/
	}

	public Porudzbina(int id, ArrayList<Vozilo> vozila, Salon salon, LocalDate datumIVreme, String trajanjeNajma,
			double cena, Korisnik kupac, StatusPorudzbine status) {
		super();
		this.id = id;
		this.vozila = vozila;
		this.salon = salon;
		this.datumIVreme = datumIVreme;
		this.trajanjeNajma = trajanjeNajma;
		this.cena = cena;
		this.kupac = kupac;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Vozilo> getVozila() {
		return vozila;
	}

	public void setVozila(ArrayList<Vozilo> vozila) {
		this.vozila = vozila;
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

	public LocalDate getDatumIVreme() {
		return datumIVreme;
	}

	public void setDatumIVreme(LocalDate datumIVreme) {
		this.datumIVreme = datumIVreme;
	}

	public String getTrajanjeNajma() {
		return trajanjeNajma;
	}

	public void setTrajanjeNajma(String trajanjeNajma) {
		this.trajanjeNajma = trajanjeNajma;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public Korisnik getKupac() {
		return kupac;
	}

	public void setKupac(Korisnik kupac) {
		this.kupac = kupac;
	}

	public StatusPorudzbine getStatus() {
		return status;
	}

	public void setStatus(StatusPorudzbine status) {
		this.status = status;
	}
}
