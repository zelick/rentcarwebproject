package model;

import enums.TipGoriva;
import enums.TipVozila;
import enums.VrstaMenjaca;

public class Vozilo {
	private int id;
	private String marka;
	private String model;
	private double cena;
	private TipVozila tip;
	private VrstaMenjaca vrsta;
	private TipGoriva tipGoriva;
	private double potrosnja;
	private int brojVrata;
	private int brojOsoba;
	private String opis;
	private String slika;
	private boolean status; //true-dostupno
	
	public Vozilo() {
		super();
		this.status = true;
	}
	
	public Vozilo(int id,String marka, String model, double cena, TipVozila tip, VrstaMenjaca vrsta, TipGoriva tipGoriva,
			double potrosnja, int brojVrata, int brojOsoba, String opis, String slika, boolean status) {
		super();
		this.id = id;
		this.marka = marka;
		this.model = model;
		this.cena = cena;
		this.tip = tip;
		this.vrsta = vrsta;
		this.tipGoriva = tipGoriva;
		this.potrosnja = potrosnja;
		this.brojVrata = brojVrata;
		this.brojOsoba = brojOsoba;
		this.opis = opis;
		this.slika = slika;
		this.status = status;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public TipVozila getTip() {
		return tip;
	}

	public void setTip(TipVozila tip) {
		this.tip = tip;
	}

	public VrstaMenjaca getVrsta() {
		return vrsta;
	}

	public void setVrsta(VrstaMenjaca vrsta) {
		this.vrsta = vrsta;
	}

	public TipGoriva getTipGoriva() {
		return tipGoriva;
	}

	public void setTipGoriva(TipGoriva tipGoriva) {
		this.tipGoriva = tipGoriva;
	}

	public double getPotrosnja() {
		return potrosnja;
	}

	public void setPotrosnja(double potrosnja) {
		this.potrosnja = potrosnja;
	}

	public int getBrojVrata() {
		return brojVrata;
	}

	public void setBrojVrata(int brojVrata) {
		this.brojVrata = brojVrata;
	}

	public int getBrojOsoba() {
		return brojOsoba;
	}

	public void setBrojOsoba(int brojOsoba) {
		this.brojOsoba = brojOsoba;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
}
