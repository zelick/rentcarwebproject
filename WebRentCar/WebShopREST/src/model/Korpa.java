package model;

import java.util.ArrayList;

public class Korpa {
	private int id;
	private ArrayList<Vozilo> vozila;
	private double cena;
	
	public Korpa() {
		super();
		vozila = new ArrayList<Vozilo>();
	}

	public Korpa(int id, ArrayList<Vozilo> vozila, double cena) {
		super();
		this.id = id;
		this.vozila = vozila;
		this.cena = cena;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Vozilo> getVozila() {
		return vozila;
	}

	public void setVozila(ArrayList<Vozilo> vozila) {
		this.vozila = vozila;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}
}
