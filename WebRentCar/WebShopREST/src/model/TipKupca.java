package model;

public class TipKupca {
	//private int id;
	private String ime;
	private int popust; //%
	private double trazeniBodovi;
	
	public TipKupca() {
		super();
		this.ime = "Bronzani";
		this.trazeniBodovi = 30000;
		this.popust = 0;
	}

	public TipKupca(String ime, int popust, double trazeniBodovi) {
		super();
		//this.id = id;
		this.ime = ime;
		this.popust = popust;
		this.trazeniBodovi = trazeniBodovi;
	}
	

	/*public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}*/

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public int getPopust() {
		return popust;
	}

	public void setPopust(int popust) {
		this.popust = popust;
	}

	public double getTrazeniBodovi() {
		return trazeniBodovi;
	}

	public void setTrazeniBodovi(double trazeniBodovi) {
		this.trazeniBodovi = trazeniBodovi;
	}
}
