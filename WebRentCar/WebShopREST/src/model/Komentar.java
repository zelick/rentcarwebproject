package model;

public class Komentar {
	private int id;
	private Korisnik korisnik;
	private Salon salon;
	private String tekst;
	private int ocena; // od 1 do 5
	private boolean vidljiv;
	private boolean odobren;
	
	public Komentar() {
		super();
		/*korisnik = new Korisnik();
		salon = new Salon();*/
		this.vidljiv = false;
		this.odobren = true;
	}
	
	public Komentar(int id, Korisnik korisnik, Salon salon, String tekst, int ocena) {
		super();
		this.id = id;
		this.korisnik = korisnik;
		this.salon = salon;
		this.tekst = tekst;
		this.ocena = ocena;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	public boolean isVidljiv() {
		return vidljiv;
	}

	public void setVidljiv(boolean vidljiv) {
		this.vidljiv = vidljiv;
	}

	public boolean isOdobren() {
		return odobren;
	}

	public void setOdobren(boolean odobren) {
		this.odobren = odobren;
	}
}
