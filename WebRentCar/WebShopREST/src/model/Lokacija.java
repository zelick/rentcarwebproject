package model;

public class Lokacija {
	private double duzina;
	private double sirina;
	private String adresa;
	
	public Lokacija() {
		super();
	}

	public Lokacija(double duzina, double sirina, String adresa) {
		super();
		this.duzina = duzina;
		this.sirina = sirina;
		this.adresa = adresa;
	}

	public double getDuzina() {
		return duzina;
	}

	public void setDuzina(double duzina) {
		this.duzina = duzina;
	}

	public double getSirina() {
		return sirina;
	}

	public void setSirina(double sirina) {
		this.sirina = sirina;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
}
