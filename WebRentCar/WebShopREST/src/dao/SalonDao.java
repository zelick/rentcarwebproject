package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import dto.SalonDto;
import enums.Pol;
import enums.TipGoriva;
import enums.TipVozila;
import enums.Uloga;
import enums.VrstaMenjaca;
import json.adapters.LocalDateTypeAdapter;
import model.Korisnik;
import model.Salon;
import model.Vozilo;

public class SalonDao {
	
	private ArrayList<Salon> saloni;
	private String contextPath;
	private DateTimeFormatter formatter;
	private String putanja;
	private Type listType;
	private ArrayList<Salon> pretraga;
	
	public SalonDao(String contextPath) {
		saloni = new ArrayList<Salon>();
		pretraga = new ArrayList<Salon>();
		this.contextPath = contextPath;
		formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
		putanja = "";
		//putanja = "../../../../../../WebShopREST/WebContent/data/saloni.json";
		//putanja = "../../../../../../rentcarwebproject/WebRentCar/WebShopREST/WebContent/data/saloni.json";
		listType = new TypeToken<ArrayList<Salon>>() {}.getType();
		loadSaloni(this.contextPath);
	}
	
	private int voziloNextId(){
		int id = -1;
		for(Salon s : saloni) {
			for(Vozilo v : s.getVozila()) {
				if(v.getId() > id) {
					id = v.getId();
				}
			}
		}
		return id + 1;
	}
	
	public int dodajVozilo(int idSalona, Vozilo vozilo) {
		vozilo.setStatus(true);
		vozilo.setId(voziloNextId());
		for(Salon s : saloni) {
			if(s.getId() == idSalona) {
				s.getVozila().add(vozilo);
				writeSaloni(contextPath);
				return vozilo.getId();
			}
		}
		return -1;
	}
	
	public Salon izmeniVoziloSalona(Vozilo izmenjenoVozilo) {
	    for (Salon s : saloni) {
	        for (int i = 0; i < s.getVozila().size(); i++) {
	            Vozilo v = s.getVozila().get(i);
	            if (v.getId() == izmenjenoVozilo.getId()) {
	                s.getVozila().set(i, izmenjenoVozilo);
	                writeSaloni(contextPath);
	                return s;
	            }
	        }
	    }
	    return null;
	}
	
	public Vozilo deleteVozilo(int voziloId) {
		 for (Salon s : saloni) {
			 for(Vozilo v: s.getVozila()) {
				 if(v.getId() == voziloId) {
					 s.getVozila().remove(v);
					 writeSaloni(contextPath);
					 return v;
				 }
			 }
		      
		 }
		return null;
	}
	
	public Vozilo getVoziloById(int voziloId) {
		for(Salon s : saloni) {
			for(Vozilo v : s.getVozila()) {
				if(v.getId() == voziloId) {
					return v;
				}
			}
		}
		return null;
	}
	
	private int nextId() {
		int  id = -1;
		for(Salon s : saloni) {
			if(s.getId() > id) {
				id = s.getId();
			}
		}
		return id + 1;
	}

	public Salon create(SalonDto salonDto) {
		Salon setSalon  = new Salon();
		setSalon.setId(nextId());
		setSalon.setNaziv(salonDto.getNaziv());
		setSalon.setRadnoVreme(salonDto.getRadnoVreme());
		setSalon.setStatus(Boolean.parseBoolean(salonDto.getStatus()));
		setSalon.setLokacija(salonDto.getLokacija());
		setSalon.setSlika(salonDto.getSlika());
		setSalon.setOcena(0);
		saloni.add(setSalon);
		writeSaloni(contextPath);
		return setSalon;
	}
	
	public ArrayList<Salon> getAll(){
        ArrayList<Salon> sortiraniSaloni = new ArrayList<Salon>();
        for(Salon s : saloni) {
            if(s.isStatus()) {
                sortiraniSaloni.add(s);
            }
        }
        for(Salon s : saloni) {
            if(!s.isStatus()) {
                sortiraniSaloni.add(s);
            }
        }
        return sortiraniSaloni;
    }
	
	public SalonDto getSalonById(int id) {
		SalonDto salon = new SalonDto();
		for(Salon s : saloni) {
			if(s.getId() == id) {
				salon.setId(Integer.toString(id));
				salon.setNaziv(s.getNaziv());
				salon.setRadnoVreme(s.getRadnoVreme());
				salon.setStatus(Boolean.toString(s.isStatus()));
				salon.setSlika(s.getSlika());
				salon.setOcena(Double.toString(s.getOcena()));
				salon.setLokacija(s.getLokacija());
				salon.setVozila(s.getVozila());
				return salon;
			}
		}
		return null;
	}
	
	
	public Salon upateStatusDostupno(int idVozila) {
		for(Salon s : saloni) {
			for(Vozilo v :s.getVozila()) {
				if(v.getId() == idVozila) {
					v.setStatus(true);
					writeSaloni(contextPath);
					return s;
				}
			}
		}
		return null;
	}
	
	public Salon updateOcenu(Salon salon) {
		for (Salon s : saloni) {
			if (s.getId() == salon.getId()) {
				s.setOcena(round(salon.getOcena(), 2));
				writeSaloni(contextPath);
				return s;
			}
		}
		return null;
	}
	
	private static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	public ArrayList<Vozilo> getDostupnaVozila(int idSalona) {
		
		ArrayList<Vozilo> dostupnaVozila = new ArrayList<>();
		for (Salon s : saloni) {
			if (s.getId() == idSalona) {
				for (Vozilo v : s.getVozila()) {
					if (v.isStatus()) {
						dostupnaVozila.add(v);
					}
				}
			}
		}
		return dostupnaVozila;
	}
	
	public ArrayList<Salon> getPretraga() {
		return pretraga;
	}
	
	public void pretrazi(String naziv, String tip, String lokacija, String ocena, String sortiranje, String filter, String samoOtvorena) {
		pretraga.clear();
		for (Salon s : saloni) {
			boolean isNaziv = naziv.isEmpty() || s.getNaziv().toLowerCase().contains(naziv.toLowerCase());
			boolean isTip = false;
			boolean isLokacija = lokacija.isEmpty() || s.getLokacija().getAdresa().toLowerCase().contains(lokacija.toLowerCase());
			boolean isOcena = ocena.isEmpty() || s.getOcena() == Double.parseDouble(ocena);
			boolean isOtvoren = !Boolean.valueOf(samoOtvorena) || (Boolean.valueOf(samoOtvorena) && s.isStatus());
			
			for (Vozilo v : s.getVozila()) {
				if (tip.equals(tip) || v.getTip() == TipVozila.valueOf(tip)) {
					isTip = true;
					break;
				}
			}
			
			boolean[] filteri = new boolean[filter.length()];
			boolean trebaFiltrirati = false;
			
			for (int i = 0; i < filter.length(); i++) {
			    char c = filter.charAt(i);
			    filteri[i] = (c == '1');
			    if (filteri[i]) {
			    	trebaFiltrirati = true;
			    }
			}
			
			boolean isFilter = true;
			if (trebaFiltrirati) {
				isFilter = false;
				for (Vozilo v : s.getVozila()) {
					boolean isVrsta = (!filteri[0] && !filteri[1]) || ((filteri[0] && v.getVrsta() == VrstaMenjaca.MANUELNI) || (filteri[1] && v.getVrsta() == VrstaMenjaca.AUTOMATIK));
					boolean isTipGoriva = (!filteri[2] && !filteri[3] && !filteri[4] && !filteri[5]) ||  ((filteri[2] && v.getTipGoriva() == TipGoriva.BENZIN) || (filteri[3] && v.getTipGoriva() == TipGoriva.DIZEL) || (filteri[4] && v.getTipGoriva() == TipGoriva.HIBRID) || (filteri[5] && v.getTipGoriva() == TipGoriva.ELEKTRICNI));
					isFilter = isVrsta && isTipGoriva;
					if(isFilter) {
						break;
					}
				}
			}
			
			if(isNaziv && isTip && isLokacija && isOcena && isOtvoren && isFilter) {
				pretraga.add(s);
			}
		}
		
		switch(sortiranje) {
			case "nazivOpadajuce":
				sortNazivObrnuto();
				break;
			case "nazivRastuce":
				sortNaziv();
				break;
			case "lokacijaOpadajuce":
				sortLokacijaObrnuto();
				break;
			case "lokacijaRastuce":
				sortLokacija();
				break;
			case "ocenaOpadajuce":
				sortOcenaObrnuto();
				break;
			case "ocenaRastuce":
				sortOcena();
				break;
			default:
				//nist ne diraj
		}
	}
	
	private void sortNaziv() {
		Comparator<Salon> compare = (Salon s1, Salon s2) -> s1.getNaziv().compareTo(s2.getNaziv());
		Collections.sort(pretraga, compare);
	}
	
	private void sortNazivObrnuto() {
		Comparator<Salon> compare = (Salon s1, Salon s2) -> s1.getNaziv().compareTo(s2.getNaziv());
		Collections.sort(pretraga, compare.reversed());
	}
	
	private void sortLokacija() {
		Comparator<Salon> compare = (Salon s1, Salon s2) -> s1.getLokacija().getAdresa().compareTo(s2.getLokacija().getAdresa());
		Collections.sort(pretraga, compare);
	}
	
	private void sortLokacijaObrnuto() {
		Comparator<Salon> compare = (Salon s1, Salon s2) -> s1.getLokacija().getAdresa().compareTo(s2.getLokacija().getAdresa());
		Collections.sort(pretraga, compare.reversed());
	}
	
	private void sortOcena() {
		Comparator<Salon> compare = (Salon s1, Salon s2) -> Double.compare(s1.getOcena(), s2.getOcena());
		Collections.sort(pretraga, compare);
	}
	
	private void sortOcenaObrnuto() {
		Comparator<Salon> compare = (Salon s1, Salon s2) -> Double.compare(s1.getOcena(), s2.getOcena());
		Collections.sort(pretraga, compare.reversed());
	}
	
	private void loadSaloni(String contextPath) {
		BufferedReader in = null;
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).setPrettyPrinting().create();
		try {
			File file = new File(contextPath + putanja);
			in = new BufferedReader(new FileReader(file));
			String json = "", line;
			
			while ((line = in.readLine()) != null) {
				json += line;
			}
			
			saloni = gson.fromJson(json, listType);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ( in != null ) {
				try {
					in.close();
				}
				catch (Exception e) { }
			}
		}
	}
	
	private void writeSaloni(String contextPath) {
	    BufferedWriter out = null;
	    Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).setPrettyPrinting().create();
	    try {
	        FileWriter file = new FileWriter(contextPath + putanja);
	        out = new BufferedWriter(file);
	        
	        String json = gson.toJson(saloni, listType);
	        
	        out.write(json);
	        out.flush();
	        
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    } finally {
	        if (out != null) {
	            try {
	                out.close();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
}
