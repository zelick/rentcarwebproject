package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import dto.KomentarDto;
import json.adapters.LocalDateTypeAdapter;
import model.Komentar;
import model.Salon;
import model.Korisnik;

public class KomentarDao {
	private ArrayList<Komentar> komentari;
	private String contextPath;
	private Type listType;
	
	public KomentarDao(String contextpath) {
		komentari = new ArrayList<Komentar>();
		this.contextPath = contextpath;
		listType = new TypeToken<ArrayList<Komentar>>() {}.getType();
		loadKomentari(this.contextPath);
	}
	
	private void test() {
		Komentar komentar = new Komentar();
		Salon salon = new Salon();
		Korisnik korisnik = new Korisnik();
		komentar.setTekst("Neki random tekst.");
		komentar.setOcena(4);
		komentar.setSalon(salon);
		komentar.setKorisnik(korisnik);
		komentari.add(komentar);
		writeKomentari(contextPath);
	}
	
	private int nextId() {
		int  id = -1;
		for(Komentar k : komentari) {
			if(k.getId() > id) {
				id = k.getId();
			}
		}
		return id + 1;
	}
	
	private Komentar komentarIzDto(KomentarDto dto) {
		Komentar k = new Komentar();
		k.setOcena(dto.getOcena());
		k.setSalon(dto.getSalon());
		k.setTekst(dto.getTekst());
		Korisnik ko = new Korisnik();
		ko.setId(Integer.parseInt(dto.getKorisnik().getId()));
		ko.setKorisnickoIme(dto.getKorisnik().getKorisnickoIme());
		k.setKorisnik(ko);
		return k;
	}
	
	public KomentarDto create(KomentarDto komentar) {
		Komentar k = komentarIzDto(komentar);
		k.setId(nextId());
		k.setVidljiv(false);
		k.setOdobren(true);
		komentari.add(k);
		writeKomentari(contextPath);
		return komentar;
	}
	
	public KomentarDto update(KomentarDto komentar) {
		for (Komentar k : komentari) {
			if (k.getId() == komentar.getId()) {
				k.setVidljiv(komentar.isVidljiv());
				k.setOdobren(komentar.isOdobren());
				writeKomentari(contextPath);
				return komentar;
			}
		}
		return null;
	}
	
	public ArrayList<Komentar> getSve(int id) {
		
		ArrayList<Komentar> komentariKorisnika = new ArrayList<Komentar>();
		
		for (Komentar k : komentari) {
			if(k.getSalon().getId() == id && k.isOdobren()) {
				komentariKorisnika.add(k);
			}
		}
		
		return komentariKorisnika;
	}
	
	public ArrayList<Komentar> getVidljive(int id) {
		
		ArrayList<Komentar> komentariKorisnika = new ArrayList<Komentar>();
		
		for (Komentar k : komentari) {
			if(k.getSalon().getId() == id && k.isVidljiv() && k.isOdobren()) {
				komentariKorisnika.add(k);
			}
		}
		
		return komentariKorisnika;
	}
	
	private void loadKomentari(String contextPath) {
		BufferedReader in = null;
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).setPrettyPrinting().create();
		try {
			File file = new File(contextPath);
			in = new BufferedReader(new FileReader(file));
			String json = "", line;
			
			while ((line = in.readLine()) != null) {
				json += line;
			}
			
			komentari = gson.fromJson(json, listType);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ( in != null ) {
				try {
					in.close();
				}
				catch (Exception e) { }
			}
		}
	}
	
	private void writeKomentari(String contextPath) {
	    BufferedWriter out = null;
	    Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).setPrettyPrinting().create();
	    try {
	        FileWriter file = new FileWriter(contextPath);
	        out = new BufferedWriter(file);
	        
	        String json = gson.toJson(komentari, listType);
	        
	        out.write(json);
	        out.flush();
	        
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    } finally {
	        if (out != null) {
	            try {
	                out.close();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
}
