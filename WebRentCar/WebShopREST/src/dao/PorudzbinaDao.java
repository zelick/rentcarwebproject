package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import dto.KorisnikDto;
import dto.PorudzbinaDto;
import enums.StatusPorudzbine;
import json.adapters.LocalDateTypeAdapter;
import model.Porudzbina;
import model.Salon;
import model.Vozilo;
import model.Korisnik;

public class PorudzbinaDao {
	
	private ArrayList<Porudzbina> porudzbine;
	private ArrayList<PorudzbinaDto> privremenePorudzbine;
	private String contextPath;
	private Type listType;
	
	public PorudzbinaDao(String contextPath) {
		porudzbine = new ArrayList<Porudzbina>();
		privremenePorudzbine = new ArrayList<PorudzbinaDto>();
		this.contextPath = contextPath;
		listType = new TypeToken<ArrayList<Porudzbina>>() {}.getType();
		loadPorudzbine(contextPath);
	}
	
	private int nextId() {
		int  id = 999999999;
		for(Porudzbina k : porudzbine) {
			if(k.getId() > id) {
				id = k.getId();
			}
		}
		return id + 1;
		
	}
	
	private Porudzbina izDto(PorudzbinaDto po) {
		Porudzbina p = new Porudzbina();
		Korisnik k = new Korisnik();
		p.setVozila(po.getVozila());
		p.setKupac(k);
		p.getKupac().setId(Integer.parseInt(po.getKupac().getId()));
		p.getKupac().setIme(po.getKupac().getIme());
		p.getKupac().setPrezime(po.getKupac().getPrezime());
		p.setCena(po.getCena());
		p.setDatumIVreme(LocalDate.parse(po.getDatumIVreme()));
		p.setTrajanjeNajma(po.getTrajanjeNajma());
		p.setSalon(po.getSalon());
		return p;
	}
	
	public PorudzbinaDto create(PorudzbinaDto porudzbina) {
		Porudzbina p = izDto(porudzbina);
		p.setId(nextId());
		p.setStatus(StatusPorudzbine.OBRADA);
		porudzbine.add(p);
		privremenePorudzbine.remove(porudzbina);
		writePorudzbine(contextPath);
		return porudzbina;
	}
	
	public PorudzbinaDto createPrivremeno(PorudzbinaDto p) {
		LocalDate pocetak = LocalDate.parse(p.getDatumIVreme());
		LocalDate kraj = LocalDate.parse(p.getKrajnjiDatum());

		int brojDana = 0;

        LocalDate currentDate = pocetak;
        while (currentDate.isBefore(kraj)) {
            brojDana++;
            currentDate = currentDate.plusDays(1);
        }
        
        p.setTrajanjeNajma(String.valueOf(brojDana));
        
        for (PorudzbinaDto po : privremenePorudzbine) {
        	if(po.getId() == p.getId()) {
        		privremenePorudzbine.remove(po);
        		break;
        	}
        }
        
		privremenePorudzbine.add(p);
		return p;
	}
	
	public PorudzbinaDto getprivremenaPorudzbina(int id) {
		for (PorudzbinaDto p : privremenePorudzbine) {
			if (p.getId() == id) {
				return p;
			}
		}
		return null;
	}
	
	public ArrayList<KorisnikDto> nadjiSumnjive(ArrayList<KorisnikDto> korisnici){
		return korisnici;
	}
	
	public ArrayList<PorudzbinaDto> getPorudzbineDtoByKupac(int idKorisnika){
		
		ArrayList<PorudzbinaDto> porudzbineDto = new ArrayList<PorudzbinaDto>();
		
		for(Porudzbina p : porudzbine) {
			if(p.getKupac().getId() == idKorisnika) {
				PorudzbinaDto dto = new PorudzbinaDto();
				dto.setId(p.getId());
				dto.setCena(p.getCena());
				dto.setDatumIVreme(String.valueOf(p.getDatumIVreme()));
				dto.setTrajanjeNajma(p.getTrajanjeNajma());
				dto.setSalon(p.getSalon());
				dto.setStatus(p.getStatus());
				
				KorisnikDto dtoKorisnik = new KorisnikDto();
				dtoKorisnik.setId(String.valueOf(p.getKupac().getId()));
				dtoKorisnik.setIme(p.getKupac().getIme());
				dtoKorisnik.setPrezime(p.getKupac().getPrezime());
				
				dto.setKupac(dtoKorisnik);
				dto.setVozila(p.getVozila());
				porudzbineDto.add(dto);
			}
		}
		
		Collections.reverse(porudzbineDto);
		return porudzbineDto;
	}
	
	public ArrayList<PorudzbinaDto> getPorudzbineDtoByMenadzer(int idSalona){
	ArrayList<PorudzbinaDto> porudzbineDto = new ArrayList<PorudzbinaDto>();
		
		for(Porudzbina p : porudzbine) {
			if(p.getSalon().getId() == idSalona) {
				PorudzbinaDto dto = new PorudzbinaDto();
				dto.setId(p.getId());
				dto.setCena(p.getCena());
				dto.setDatumIVreme(String.valueOf(p.getDatumIVreme()));
				dto.setTrajanjeNajma(p.getTrajanjeNajma());
				dto.setSalon(p.getSalon());
				dto.setStatus(p.getStatus());
				
				KorisnikDto dtoKorisnik = new KorisnikDto();
				dtoKorisnik.setId(String.valueOf(p.getKupac().getId()));
				dtoKorisnik.setIme(p.getKupac().getIme());
				dtoKorisnik.setPrezime(p.getKupac().getPrezime());
				
				dto.setKupac(dtoKorisnik);
				dto.setVozila(p.getVozila());
				porudzbineDto.add(dto);
			}
		}
		
		return porudzbineDto;
	}
	
	public Porudzbina update(PorudzbinaDto porudzbina) {
		
		for (Porudzbina p : porudzbine) {
			if (p.getId() == porudzbina.getId()) {
				p.setStatus(porudzbina.getStatus());
				writePorudzbine(contextPath);
				return p;
			}
		}
		return null;
	}
	
	public Porudzbina prihvatiPoruzbinu(int idPorudzbine) {
		for(Porudzbina p : porudzbine) {
			if(p.getId() == idPorudzbine) {
				p.setStatus(StatusPorudzbine.ODOBRENO);
				writePorudzbine(contextPath);
				return p;
			}
		}
		return null;
	}
	
	public Porudzbina odbijPoruzbinu(int idPorudzbine) {
		for(Porudzbina p : porudzbine) {
			if(p.getId() == idPorudzbine) {
				p.setStatus(StatusPorudzbine.ODBIJENO);
				writePorudzbine(contextPath);
				return p;
			}
		}
		return null;
	}
	
	public Porudzbina preuzmiPorudzbinu(int idPorudzbine) {
		for(Porudzbina p : porudzbine) {
			if(p.getId() == idPorudzbine) {
				p.setStatus(StatusPorudzbine.PREUZETO);
				writePorudzbine(contextPath);
				return p;
			}
		}
		return null;
	}
	
	public Porudzbina vratiPorudzbinu(int idPorudzbine) {
		for(Porudzbina p : porudzbine) {
			if(p.getId() == idPorudzbine) {
				p.setStatus(StatusPorudzbine.VRACENO);
				writePorudzbine(contextPath);
				return p;
			}
		}
		return null;
	}
	
	public ArrayList<Porudzbina> getPorudzbineByKupac(int id) {
		
		ArrayList<Porudzbina> porudzbineKupca = new ArrayList<Porudzbina>();
		for (Porudzbina p : porudzbine) {
			if(id == p.getKupac().getId()) {
				porudzbineKupca.add(p);
			}
		}
		return porudzbineKupca;
	}
	
	public ArrayList<Porudzbina> getPorudzbineByMenadzer(int idSalona) {
		
		ArrayList<Porudzbina> porudzbineKupca = new ArrayList<Porudzbina>();
		for (Porudzbina p : porudzbine) {
			if(idSalona == p.getSalon().getId()) {
				porudzbineKupca.add(p);
			}
		}
		return porudzbineKupca;
	}
	
	public boolean isSlobodan(String pocetniDatum, String krajnjiDatum, int idVozila) {
		LocalDate pocetni = LocalDate.parse(pocetniDatum);
		LocalDate krajnji = LocalDate.parse(krajnjiDatum);
		
		for (Porudzbina p : porudzbine) {
			
			if (p.getStatus() == StatusPorudzbine.OBRADA || p.getStatus() == StatusPorudzbine.ODOBRENO) {
				LocalDate datumNajma = p.getDatumIVreme();
				LocalDate krajNajma = datumNajma.plusDays(Long.parseLong(p.getTrajanjeNajma()));
			
				for (Vozilo v : p.getVozila()) {
					if (v.getId() == idVozila) {
						if ((datumNajma.isBefore(pocetni) && krajNajma.isAfter(pocetni)) || (datumNajma.isAfter(pocetni) && datumNajma.isBefore(krajnji))) {
						
							return false;
						}
					}
				}
			}
		}
		
		return true;
	}
	
	private ArrayList<PorudzbinaDto> pretvoriDto(ArrayList<Porudzbina> rezultat){
		ArrayList<PorudzbinaDto> rezultatDto = new ArrayList<PorudzbinaDto>();
		
		for(Porudzbina p : rezultat) {
			PorudzbinaDto dto = new PorudzbinaDto();
			dto.setId(p.getId());
			dto.setCena(p.getCena());
			dto.setDatumIVreme(String.valueOf(p.getDatumIVreme()));
			dto.setTrajanjeNajma(p.getTrajanjeNajma());
			dto.setSalon(p.getSalon());
			dto.setStatus(p.getStatus());
			
			KorisnikDto dtoKorisnik = new KorisnikDto();
			dtoKorisnik.setId(String.valueOf(p.getKupac().getId()));
			dtoKorisnik.setIme(p.getKupac().getIme());
			dtoKorisnik.setPrezime(p.getKupac().getPrezime());
			
			dto.setKupac(dtoKorisnik);
			dto.setVozila(p.getVozila());
			rezultatDto.add(dto);
		}
		
		return rezultatDto;
	}
	
	
	public ArrayList<PorudzbinaDto> pretragaPorudzbineMenadzer(String pocetniDatum, String krajnjiDatum,
			String cenaOd, String cenaDo, String selectedSort,int idSalona){
		
		ArrayList<Porudzbina> rezultat = new ArrayList<Porudzbina>();
		
		for(Porudzbina p : porudzbine) {
			if(p.getSalon().getId() == idSalona) {
				
	            LocalDate porudzbinaKraj = p.getDatumIVreme().plusDays(Long.parseLong(p.getTrajanjeNajma()));
				boolean isCenaOd = cenaOd.isEmpty() || p.getCena() >= Double.parseDouble(cenaOd);
				boolean idCenaDo = cenaDo.isEmpty() || p.getCena() <= Double.parseDouble(cenaDo);
				boolean isPocetniDatum = pocetniDatum.isEmpty() || (LocalDate.parse(pocetniDatum).isAfter(p.getDatumIVreme()) && LocalDate.parse(pocetniDatum).isBefore(porudzbinaKraj));
				boolean isKrajnjiDatum = krajnjiDatum.isEmpty() || (LocalDate.parse(krajnjiDatum).isAfter(p.getDatumIVreme()) && LocalDate.parse(krajnjiDatum).isBefore(porudzbinaKraj));
				
				if(isCenaOd && idCenaDo && isPocetniDatum && isKrajnjiDatum) {
					rezultat.add(p);
				}
			}
		}
		
		Comparator<Porudzbina> compare = null;
		
		switch(selectedSort) {
		case "cenaRastuce":
			compare = (Porudzbina p1, Porudzbina p2) -> Double.compare(p1.getCena(), p2.getCena());
			Collections.sort(rezultat, compare);
			break;
		case "cenaOpadajuce":
			compare = (Porudzbina p1, Porudzbina p2) -> Double.compare(p1.getCena(), p2.getCena());
			Collections.sort(rezultat,  compare.reversed());
			break;
		case "datumRastuce":
			compare = (Porudzbina p1, Porudzbina p2) -> p1.getDatumIVreme().compareTo(p2.getDatumIVreme());
			Collections.sort(rezultat, compare);
			break;
		case "datumOpadajuce":
			compare = (Porudzbina p1, Porudzbina p2) -> p1.getDatumIVreme().compareTo(p2.getDatumIVreme());
			Collections.sort(rezultat, compare.reversed());
			break;
		default:
			//nist ne diraj
		}
		
		ArrayList<PorudzbinaDto> rezultatDto = pretvoriDto(rezultat);
		return rezultatDto;
	}
	
	
	
	
	public ArrayList<PorudzbinaDto> pretragaPorudzbineKorisnik(String nazivSalona, String pocetniDatum, String krajnjiDatum,
			String cenaOd, String cenaDo, String selectedSort,int idKorisnika){
		
		ArrayList<Porudzbina> rezultat = new ArrayList<Porudzbina>();
		
		for(Porudzbina p : porudzbine) {
			if(p.getKupac().getId() == idKorisnika) {
				
	            LocalDate porudzbinaKraj = p.getDatumIVreme().plusDays(Long.parseLong(p.getTrajanjeNajma()));
				
				boolean isNazivSalona = nazivSalona.isEmpty() || p.getSalon().getNaziv().toLowerCase().contains(nazivSalona.toLowerCase());
				boolean isCenaOd = cenaOd.isEmpty() || p.getCena() >= Double.parseDouble(cenaOd);
				boolean idCenaDo = cenaDo.isEmpty() || p.getCena() <= Double.parseDouble(cenaDo);
				boolean isPocetniDatum = pocetniDatum.isEmpty() || (LocalDate.parse(pocetniDatum).isAfter(p.getDatumIVreme()) && LocalDate.parse(pocetniDatum).isBefore(porudzbinaKraj));
				boolean isKrajnjiDatum = krajnjiDatum.isEmpty() || (LocalDate.parse(krajnjiDatum).isAfter(p.getDatumIVreme()) && LocalDate.parse(krajnjiDatum).isBefore(porudzbinaKraj));
				
				if(isNazivSalona && isCenaOd && idCenaDo && isPocetniDatum && isKrajnjiDatum) {
					rezultat.add(p);
				}
			}
		}
		
		Comparator<Porudzbina> compare = null;
		
		switch(selectedSort) {
		case "nazivOpadajuce":
			compare = (Porudzbina p1, Porudzbina p2) -> p1.getSalon().getNaziv().compareTo(p2.getSalon().getNaziv());
			Collections.sort(rezultat, compare.reversed());
			break;
		case "nazivRastuce":
			compare = (Porudzbina p1, Porudzbina p2) -> p1.getSalon().getNaziv().compareTo(p2.getSalon().getNaziv());
			Collections.sort(rezultat, compare);
			break;
		case "cenaRastuce":
			compare = (Porudzbina p1, Porudzbina p2) -> Double.compare(p1.getCena(), p2.getCena());
			Collections.sort(rezultat, compare);
			break;
		case "cenaOpadajuce":
			compare = (Porudzbina p1, Porudzbina p2) -> Double.compare(p1.getCena(), p2.getCena());
			Collections.sort(rezultat,  compare.reversed());
			break;
		case "datumRastuce":
			compare = (Porudzbina p1, Porudzbina p2) -> p1.getDatumIVreme().compareTo(p2.getDatumIVreme());
			Collections.sort(rezultat, compare);
			break;
		case "datumOpadajuce":
			compare = (Porudzbina p1, Porudzbina p2) -> p1.getDatumIVreme().compareTo(p2.getDatumIVreme());
			Collections.sort(rezultat, compare.reversed());
			break;
		default:
			//nist ne diraj
		}
		
		ArrayList<PorudzbinaDto> rezultatDto = pretvoriDto(rezultat);
		return rezultatDto;
	}
	
	public boolean isSumljiv(int id) {
		
        LocalDate currentDate = LocalDate.now();
        
        LocalDate minDate = currentDate;
        LocalDate maksDate = currentDate;
        
        for (Porudzbina p : porudzbine) {
        	if (p.getStatus() == StatusPorudzbine.OTKAZANO) {
        		if (!p.getDatumIVreme().isAfter(minDate)) {
        			minDate = p.getDatumIVreme();
        		}
        		if (!p.getDatumIVreme().isBefore(maksDate)) {
        			maksDate = p.getDatumIVreme();
        		}
        	}
        }
        
        /*for (LocalDate iter = minDate; !iter.isAfter(maksDate.minusDays(30)); iter.plusDays(1)) { */
        LocalDate iter = minDate;
        do {
        	int cancellationCount = 0;
        	for (Porudzbina p : porudzbine) {
        		if (p.getKupac().getId() == id && p.getStatus() == StatusPorudzbine.OTKAZANO && (!p.getDatumIVreme().isBefore(iter)) && !p.getDatumIVreme().isAfter(iter.plusDays(30))) {
				
		        
        			cancellationCount++;
        		}
        	}
        	
        	if(cancellationCount > 5) {
        		return true;
        	}
        	
        	iter.plusDays(1);
        } while (!iter.isAfter(maksDate.minusDays(30)));
		
		return false;
	}
	
	private void loadPorudzbine(String contextPath) {
		BufferedReader in = null;
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).setPrettyPrinting().create();
		try {
			File file = new File(contextPath);
			in = new BufferedReader(new FileReader(file));
			String json = "", line;
			
			while ((line = in.readLine()) != null) {
				json += line;
			}
			
			porudzbine = gson.fromJson(json, listType);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ( in != null ) {
				try {
					in.close();
				}
				catch (Exception e) { }
			}
		}
	}
	
	private void writePorudzbine(String contextPath) {
	    BufferedWriter out = null;
	    Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).setPrettyPrinting().create();
	    try {
	        FileWriter file = new FileWriter(contextPath);
	        out = new BufferedWriter(file);
	        
	        String json = gson.toJson(porudzbine, listType);
	        
	        out.write(json);
	        out.flush();
	        
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    } finally {
	        if (out != null) {
	            try {
	                out.close();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
}
