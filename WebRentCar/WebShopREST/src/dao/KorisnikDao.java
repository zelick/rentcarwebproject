package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import dto.KorisnikDto;
import enums.Pol;
import enums.StatusPorudzbine;
import enums.Uloga;
import json.adapters.LocalDateTypeAdapter;
import model.Korisnik;
import model.Korpa;
import model.Porudzbina;
import model.Salon;
import model.Vozilo;
import model.TipKupca;

public class KorisnikDao {
	
	private ArrayList<Korisnik> korisnici;
	private String contextPath;
	private String putanja;
	private Type listType;
	private ArrayList<Korisnik> pretraga;
	private ArrayList<Vozilo> dostupnaVozila;
	
	private int idUlogovanogKorisnika;
	
	public KorisnikDao(String contextPath) {
		korisnici = new ArrayList<Korisnik>();
		pretraga = new ArrayList<Korisnik>();
		dostupnaVozila = new ArrayList<>();
		this.contextPath = contextPath;
		putanja = "";
		//putanja = "../../../../../../WebShopREST/WebContent/data/korisnici.json";
		//putanja = "../../../../../../rentcarwebproject/WebRentCar/WebShopREST/WebContent/data/korisnici.json";
		listType = new TypeToken<ArrayList<Korisnik>>() {}.getType();
		loadKorisnici(contextPath);
	}
	
	
	private int nextId() {
		int  id = -1;
		for(Korisnik k : korisnici) {
			if(k.getId() > id) {
				id = k.getId();
			}
		}
		return id + 1;
	}
	
	public Korisnik blokirajKorisnika(int idKorisnika) {
		for(Korisnik k : korisnici) {
			if(k.getId() == idKorisnika) {
				k.setBlokiran(true);
				writeKorisnici(contextPath);
				return k;
			}
		}
		return null;
	}
	
	private int voziloNextId(){
		int id = -1;
		for(Korisnik k : korisnici) {
			for(Vozilo v : k.getSalon().getVozila()) { 
				if(v.getId() > id) {
					id = v.getId();
				}
			}
		}
		return id + 1;
	}
	
	public Vozilo deleteVozilo(int voziloId) {
		 for (Korisnik k : korisnici) {
			 for(Vozilo v: k.getSalon().getVozila()) {
				 if(v.getId() == voziloId) {
					 k.getSalon().getVozila().remove(v);
					 writeKorisnici(contextPath);
					 return v;
				 }
			 }
		      
		 }
		return null;
	}
	
	public Korisnik dodajVozilo(int korisnikId,Vozilo vozilo) {
		vozilo.setStatus(true);
		vozilo.setId(voziloNextId());
		for(Korisnik k : korisnici) {
			if(k.getId() == korisnikId) {
				k.getSalon().getVozila().add(vozilo);
				writeKorisnici(contextPath);
				return k;
			}
		}
		return null;
	}
	
	public Korisnik izmeniVoziloKorisniku(Vozilo izmenjenoVozilo) {
		for(Korisnik k : korisnici) {
			 Salon s = k.getSalon();
			 for (int i = 0; i < s.getVozila().size(); i++) {
		            Vozilo v = s.getVozila().get(i);
		            if (v.getId() == izmenjenoVozilo.getId()) {
		                s.getVozila().set(i, izmenjenoVozilo);
		                writeKorisnici(contextPath);
		                return k;
		            }
		     }
		}
		return null;
	}
	
	public ArrayList<KorisnikDto> getPretaragaKorisniciDTO(){
		ArrayList<KorisnikDto> korisniciDTO = new ArrayList<KorisnikDto>();
		for(Korisnik k : pretraga) {
			KorisnikDto dtoKorisnik = new KorisnikDto();
			dtoKorisnik.setId(Integer.toString(k.getId()));
			dtoKorisnik.setIme(k.getIme());
			dtoKorisnik.setPrezime(k.getPrezime());
			dtoKorisnik.setKorisnickoIme(k.getKorisnickoIme());
			dtoKorisnik.setLozinka(k.getLozinka());
			dtoKorisnik.setPol(k.getPol().name());
			dtoKorisnik.setDatumRodjenja(k.getDatumRodjenja().toString());
			dtoKorisnik.setUloga(k.getUloga().toString());
			//dtoKorisnik.setSvePorudzbine(k.getSvePorudzbine());
			dtoKorisnik.setKorpa(k.getKorpa());
			dtoKorisnik.setSalon(k.getSalon());
			dtoKorisnik.setBodovi(k.getBodovi());
			dtoKorisnik.setTipKupca(k.getTipKupca());
			korisniciDTO.add(dtoKorisnik);
		}	
		return korisniciDTO;
	}
	
	
	public void pretrazi(String korsnickoIme, String ime, String prezime, String sort, String ulogaKupac, String ulogaMenadzer, String ulogaAdministrator) {
		
		boolean isKupac = ulogaKupac.equals("1"); //true ako je 1, false ako nije 1
		boolean isMenadzer = ulogaMenadzer.equals("1");
		boolean isAdministrator = ulogaAdministrator.equals("1");
		
		pretraga.clear();
		
		for (Korisnik k : korisnici) {
			
			boolean isKorisnickoIme = korsnickoIme.isEmpty() || k.getKorisnickoIme().toLowerCase().contains(korsnickoIme.toLowerCase());
			boolean isIme = ime.isEmpty() || k.getIme().toLowerCase().contains(ime.toLowerCase());
			boolean isPrezime = prezime.isEmpty() || k.getPrezime().toLowerCase().contains(prezime.toLowerCase());
		
			boolean isUlogaMatch = false;
			boolean proveraPrazno = (!isKupac && !isMenadzer && !isAdministrator);
			boolean proveraKupac = (isKupac && (k.getUloga() == Uloga.KUPAC));
			boolean proveraMenzader = (isMenadzer && (k.getUloga() == Uloga.MENADZER));
		    boolean proveraAdministrator = (isAdministrator && (k.getUloga() == Uloga.ADMINISTRATOR));
			
			if (proveraPrazno || proveraKupac || proveraMenzader || proveraAdministrator) {
				isUlogaMatch = true;
			}

			if (isKorisnickoIme && isIme && isPrezime && isUlogaMatch) {
				pretraga.add(k);
			}
		}
		
		switch(sort) {
		case "korisnickoImeRastuce":
			sortKorisnickoImeRastuce();
			break;
		case "korisnickoImeOpadajuce":
			sortKorisnickoImeOpadajuce();
			break;
		case "imeRastuce":
			sortImeRastuce();
			break;
		case "imeOpdajuce":
			sortImeOpadajuce();
			break;
		case "prezimeRastuce":
			sortPrezimeRastuce();
			break;
		case "prezimeOpadajuce":
			sortPrezimeOpadajuce();
			break;
		case "bodoviRastuce":
			sortBodoviRastuce();
			break;
		case "bodoviOpadajuce":
			sortBodoviOpadajuce();
			break;
		default:
		}	
	}
	
	public void sortKorisnickoImeRastuce() {
		Comparator<Korisnik> compare = (Korisnik k1, Korisnik k2) -> k1.getKorisnickoIme().compareTo(k2.getKorisnickoIme());
		Collections.sort(pretraga, compare);
	}
	
	public void sortKorisnickoImeOpadajuce() {
		Comparator<Korisnik> compare = (Korisnik k1, Korisnik k2) -> k1.getKorisnickoIme().compareTo(k2.getKorisnickoIme());
		Collections.sort(pretraga,  compare.reversed());
	}
	
	public void sortImeRastuce() {
		Comparator<Korisnik> compare = (Korisnik k1, Korisnik k2) -> k1.getIme().compareTo(k2.getIme());
		Collections.sort(pretraga, compare);
	}
	
	public void sortImeOpadajuce() {
		Comparator<Korisnik> compare = (Korisnik k1, Korisnik k2) -> k1.getIme().compareTo(k2.getIme());
		Collections.sort(pretraga,  compare.reversed());
	}
	
	public void sortPrezimeRastuce() {
		Comparator<Korisnik> compare = (Korisnik k1, Korisnik k2) -> k1.getPrezime().compareTo(k2.getPrezime());
		Collections.sort(pretraga, compare);
	}
	
	public void sortPrezimeOpadajuce() {
		Comparator<Korisnik> compare = (Korisnik k1, Korisnik k2) -> k1.getPrezime().compareTo(k2.getPrezime());
		Collections.sort(pretraga,  compare.reversed());
	}
	
	public void sortBodoviRastuce() {
		Comparator<Korisnik> compare = (Korisnik k1, Korisnik k2) -> Double.compare(k1.getBodovi(), k2.getBodovi());
		Collections.sort(pretraga, compare);
	}
	
	public void sortBodoviOpadajuce() {
		Comparator<Korisnik> compare = (Korisnik k1, Korisnik k2) -> Double.compare(k1.getBodovi(), k2.getBodovi());
		Collections.sort(pretraga,  compare.reversed());
	}
	
	public KorisnikDto find(String korisnickoIme, String lozinka) {
		
		for(Korisnik k : korisnici) {
			if(k.getKorisnickoIme().equals(korisnickoIme) && k.getLozinka().equals(lozinka) && !k.isBlokiran()) {
				
				return korisnikUDto(k);
			}
		}
		return null;
	}
	
	public Korisnik create(KorisnikDto korisnik) {
		Korisnik setKorisnik = new Korisnik();
		setKorisnik.setId(nextId());
		setKorisnik.setKorisnickoIme(korisnik.getKorisnickoIme());
		setKorisnik.setLozinka(korisnik.getLozinka());
		setKorisnik.setIme(korisnik.getIme());
		setKorisnik.setPrezime(korisnik.getPrezime());
		setKorisnik.setPol(Pol.valueOf(korisnik.getPol()));
		setKorisnik.setDatumRodjenja(LocalDate.parse(korisnik.getDatumRodjenja()));
		setKorisnik.setUloga(Uloga.valueOf(korisnik.getUloga()));
		korisnici.add(setKorisnik);
		idUlogovanogKorisnika = setKorisnik.getId(); //ulgovan korisnik	
		writeKorisnici(contextPath);
		return setKorisnik;
	}
	
	public ArrayList<Korisnik> getAll(){
		return korisnici;
	}
	
	//za prikaz 
	public ArrayList<KorisnikDto> getAllDTO(){
		ArrayList<KorisnikDto> korisniciDTO = new ArrayList<KorisnikDto>();
		for(Korisnik k : korisnici) {
			KorisnikDto dtoKorisnik = new KorisnikDto();
			dtoKorisnik.setId(Integer.toString(k.getId()));
			dtoKorisnik.setIme(k.getIme());
			dtoKorisnik.setPrezime(k.getPrezime());
			dtoKorisnik.setKorisnickoIme(k.getKorisnickoIme());
			dtoKorisnik.setLozinka(k.getLozinka());
			dtoKorisnik.setPol(k.getPol().name());
			dtoKorisnik.setDatumRodjenja(k.getDatumRodjenja().toString());
			dtoKorisnik.setUloga(k.getUloga().toString());
			//dtoKorisnik.setSvePorudzbine(k.getSvePorudzbine());
			dtoKorisnik.setKorpa(k.getKorpa());
			dtoKorisnik.setSalon(k.getSalon());
			dtoKorisnik.setBodovi(k.getBodovi());
			dtoKorisnik.setTipKupca(k.getTipKupca());
			korisniciDTO.add(dtoKorisnik);
		}
		return korisniciDTO;
	}
	
	public KorisnikDto getById(int id) {
		for(Korisnik k : korisnici) {
			if(k.getId() == id) {
				
				return korisnikUDto(k);
			}
		}
		return null;
	}
	
	public Korisnik getByUserName(String userName) {
		for(Korisnik k : korisnici) {
			if(k.getKorisnickoIme().equals(userName)) {
				return k;
			}
		}
		return null;
	}
	
	public boolean isUsernameExists(String ime) {
		for(Korisnik k : korisnici) {
			if(k.getKorisnickoIme().equals(ime)) {
				return false;
			}
		}
		return true;
	}
	
	public ArrayList<Vozilo> getSvaVozila(){
		ArrayList<Vozilo> vozila = new ArrayList<Vozilo>();
		for(Korisnik k : korisnici) {
			for(Vozilo v : k.getSalon().getVozila()) {
				vozila.add(v);
			}
		}
		return vozila;
	}
	
	/*poruzbine*/
	
	public double izracunajCenuPorudzbine(String pocetniDatum, String krajnjiDatum,int idKorisnika) {
		 LocalDate pocetni = LocalDate.parse(pocetniDatum);
		 LocalDate krajnji = LocalDate.parse(krajnjiDatum);
		 int brojDana = 0;
		 
		 LocalDate currentDate = pocetni;
		 while (currentDate.isBefore(krajnji)) {
			 brojDana++;
		     currentDate = currentDate.plusDays(1);
		 }
		 
		 double suma = 0;
		 for(Korisnik k : korisnici) {
			 if(k.getId() == idKorisnika){
				 for(Vozilo v : k.getKorpa().getVozila()){
					 suma += v.getCena() * brojDana;
				 }
			 }
		 }
		 return suma;
	}
	
	public Porudzbina kreirajPorudzbinu(String pocetniDatum, String krajnjiDatum, double ukupnaCena,Korisnik korisnik) {
		 LocalDate pocetni = LocalDate.parse(pocetniDatum);
		 LocalDate krajnji = LocalDate.parse(krajnjiDatum);
		 int brojDana = 0;
		 
		 LocalDate currentDate = pocetni;
		 while (currentDate.isBefore(krajnji)) {
			 brojDana++;
		     currentDate = currentDate.plusDays(1);
		 }
		
		for(Korisnik k : korisnici) {
			if(k.getId() == korisnik.getId()) {
				Porudzbina p = new Porudzbina();
				//sta sa id?
				p.setCena(ukupnaCena);
				p.setDatumIVreme(currentDate);
				//zasto ima kupca?
				p.setDatumIVreme(pocetni);
				p.setTrajanjeNajma(String.valueOf(brojDana)); //ovo izmeni na broj?
				//p.setSalon() sta sa ovim
				p.setStatus(StatusPorudzbine.OBRADA);
				p.setVozila(k.getKorpa().getVozila());
				//k.getSvePorudzbine().add(p);
				writeKorisnici(contextPath);
				return p;
			}
		}
		
		return null;
	}
	
	public Korpa isprazniKorpu(int korisnikId) {
		for(Korisnik k : korisnici) {
			if(k.getId() == korisnikId) {
				k.getKorpa().getVozila().clear();
				k.getKorpa().setCena(0);	
				writeKorisnici(contextPath);
				return k.getKorpa();
			}
		}
		return null;
	}
	
	private int sumaCeneVozila(Korpa k) {
		int suma = 0;
		for(Vozilo v : k.getVozila()) {
			 suma += v.getCena();
		}
		return suma;
	}
	
	
	public Korpa dodajVoziloUKorpu(int idKorisnika, Vozilo vozilo) {
		for(Korisnik k : korisnici) {
			if(k.getId() == idKorisnika) {
				//sta ovde sa id?
				k.getKorpa().getVozila().add(vozilo);
				k.getKorpa().setCena(sumaCeneVozila(k.getKorpa()));	
				writeKorisnici(contextPath);
				return k.getKorpa();
			}
		}
		return null;
	}
	
	public Korpa getKorpaKorisnika(int idKorisnika) {
		for(Korisnik k : korisnici) {
			if(k.getId() == idKorisnika) {
				return k.getKorpa();
			}
		}
		return null;
	}
	
	public ArrayList<Vozilo> getDostupnaVozila(){
		return dostupnaVozila;
	}
	
	public Korisnik delete(Korisnik korisnik){
		Korisnik izbrisi = null;
		
		for(Korisnik k: korisnici) {
			if(k.getId() == korisnik.getId()) {
				izbrisi = k;
				break;
			}
		}
		
		korisnici.remove(izbrisi);
		return izbrisi;
	}
	
	public KorisnikDto update(KorisnikDto korisnik) {
		
		for(Korisnik k : korisnici) {
			if(k.getId() == Integer.parseInt(korisnik.getId())) {
				k.setIme(korisnik.getIme());
				k.setPrezime(korisnik.getPrezime());
				k.setKorisnickoIme(korisnik.getKorisnickoIme());
				k.setLozinka(korisnik.getLozinka());
				k.setDatumRodjenja(LocalDate.parse(korisnik.getDatumRodjenja()));
				
				writeKorisnici(contextPath);
				return korisnikUDto(k);
			}
		}
		return null;
	}
	
	public Korisnik updateBodovi(KorisnikDto korisnik) {
		
		for(Korisnik k : korisnici) {
			if(k.getId() == Integer.parseInt(korisnik.getId())) {
				
				double bodovi = korisnik.getBodovi();
				if (bodovi < 0) {
					bodovi = 0;
				}
				
				k.setBodovi(bodovi);
				
				double trazeniBodovi = k.getTipKupca().getTrazeniBodovi();
					
				if (bodovi < 30000) {
					k.getTipKupca().setIme("Bronzani");
					k.getTipKupca().setTrazeniBodovi(30000);
					k.getTipKupca().setPopust(0);
				}
				else if (bodovi < 50000) {
					k.getTipKupca().setIme("Srebrni");
					k.getTipKupca().setTrazeniBodovi(50000);
					k.getTipKupca().setPopust(3);
				}
				else if (bodovi < 70000) {
					k.getTipKupca().setIme("Zlatni");
					k.getTipKupca().setTrazeniBodovi(70000);
					k.getTipKupca().setPopust(5);
				}
				
				writeKorisnici(contextPath);
				return k;
			}
		}
		return null;
	}
	
	public KorisnikDto updateOcenuObjekta(KorisnikDto korisnik) {
		for (Korisnik k : korisnici) {
			if(k.getId() == Integer.parseInt(korisnik.getId())) {
				k.getSalon().setOcena(round(korisnik.getSalon().getOcena(), 2));
				writeKorisnici(contextPath);
				return korisnik;
			}
		}
		return null;
	}
	
	private static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	public boolean isUsernameAndPasswordExists(String korisnickoIme, String lozinka) {
		for(Korisnik k : korisnici) {
			if(k.getKorisnickoIme().equals(korisnickoIme) && k.getLozinka().equals(lozinka)) {
				idUlogovanogKorisnika = k.getId(); //postavi id ulogovanog korisnika
				return true;
			}
		}
		return false;
	}
	
	public int getLoggedUser() {
		return idUlogovanogKorisnika;
	}
	
	public KorisnikDto dodajSalonMenadzeru(int id, Salon salon) {
		for (Korisnik k : korisnici) {
			if(k.getId() == id) {
				k.setSalon(salon);
				writeKorisnici(contextPath);
				return korisnikUDto(k);
			}
		}
		return null;
	}
	
	public ArrayList<KorisnikDto> getSlobodneMenadzere() {
		ArrayList<KorisnikDto> menadzeri = new ArrayList<KorisnikDto>();
		for (Korisnik k : korisnici) {
			if(menadzerNemaSalon(k)) {
				menadzeri.add(korisnikUDto(k));
			}
		}
		return menadzeri;
	}
	
	public boolean isMenadzerSlobodan() {
		for (Korisnik k : korisnici) {
			if (menadzerNemaSalon(k)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean menadzerNemaSalon(Korisnik k) {
		return k.getUloga() == Uloga.MENADZER && k.getSalon().getNaziv() == null;
	}
	
	private KorisnikDto korisnikUDto(Korisnik k) {
		KorisnikDto dtoKorisnik = new KorisnikDto();
		dtoKorisnik.setId(Integer.toString(k.getId()));
		dtoKorisnik.setIme(k.getIme());
		dtoKorisnik.setPrezime(k.getPrezime());
		dtoKorisnik.setKorisnickoIme(k.getKorisnickoIme());
		dtoKorisnik.setLozinka(k.getLozinka());
		dtoKorisnik.setPol(k.getPol().name());
		dtoKorisnik.setDatumRodjenja(k.getDatumRodjenja().toString());
		dtoKorisnik.setUloga(k.getUloga().toString());
		//dtoKorisnik.setSvePorudzbine(k.getSvePorudzbine());
		dtoKorisnik.setKorpa(k.getKorpa());
		dtoKorisnik.setSalon(k.getSalon());
		dtoKorisnik.setBodovi(k.getBodovi());
		dtoKorisnik.setTipKupca(k.getTipKupca());
		return dtoKorisnik;
	}
	
	public Korisnik upateStatusDostupno(int idVozila) {
		for(Korisnik k : korisnici) {
			for(Vozilo v : k.getSalon().getVozila()) {
				if(v.getId() == idVozila) {
					v.setStatus(true);
					writeKorisnici(contextPath);
					return k;
				}
			}
		}
		return null;
	}
	
	private void loadKorisnici(String contextPath) {
		BufferedReader in = null;
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).setPrettyPrinting().create();
		try {
			File file = new File(contextPath + putanja);
			in = new BufferedReader(new FileReader(file));
			String json = "", line;
			
			while ((line = in.readLine()) != null) {
				json += line;
			}
			
			korisnici = gson.fromJson(json, listType);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ( in != null ) {
				try {
					in.close();
				}
				catch (Exception e) { }
			}
		}
	}
	
	private void writeKorisnici(String contextPath) {
	    BufferedWriter out = null;
	    Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter()).setPrettyPrinting().create();
	    try {
	        FileWriter file = new FileWriter(contextPath + putanja);
	        out = new BufferedWriter(file);
	        
	        String json = gson.toJson(korisnici, listType);
	        
	        out.write(json);
	        out.flush();
	        
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    } finally {
	        if (out != null) {
	            try {
	                out.close();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
}
